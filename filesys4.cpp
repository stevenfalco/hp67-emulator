// HP67u Calculator Emulator - Filesystem
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "filesys4.h"

filesys4::filesys4()
{
	//DEBUG_printf("filesystem 4 constructed");
}

filesys4::~filesys4()
{
	//DEBUG_printf("filesystem 4 destroyed");
}

// Load a file and return it.
//
// This will be something like:
// 
// SIDE1
// 38
// 0000000
// 0000000
// 0000000
// 3100222
// 0e30fa1
// c3001fc
// 4737313
// ...
// SIDE2
// 0
// CARD
// 11
// Title: a2-1_polar_complex_four_functions
// A: X<>Y
// B: Z1+Z2
// C: 1/Z1
// D: Z1*Z2
// E: 
// a: 
// ...
// HELP
// 1
// END
QString
filesys4::load(QString name)
{
	QFile file(name);

	if(file.open(QIODevice::ReadOnly)) {
		QTextStream in(&file);
		QString s = in.readAll();
		file.close();

		return s;
	}

	return "";
}

// Save to a file.
void
filesys4::save(QString name, QString s)
{
	QFile file(name);

	if(file.open(QIODevice::WriteOnly)) {
		QTextStream out(&file);
		out << s;
		file.close();
	}
}


// HP67u Calculator Emulator
//
// File->Magcard->Card Access
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_3.h"
#include "ui_page_3.h"

#include "skin.h"

Page_3::Page_3(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Page_3)
{
	ui->setupUi(this);

	QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
	QObject::connect(ui->Side1, SIGNAL(clicked()), this, SLOT(side()));
	QObject::connect(ui->Side2, SIGNAL(clicked()), this, SLOT(side()));
	QObject::connect(ui->newFile, SIGNAL(clicked()), this, SLOT(chooseFile()));
}

Page_3::~Page_3()
{
	delete ui;
}

// Cards are a little confusing, because while they have two sides, we store
// them in a single file.  We always read or write the entire card file on
// disk, but only the data from the appropriate sides are used.
void 
Page_3::init(CLASSES *cl, QString fn)
{
	// Save pointers to the various classes.
	m_cl = cl;

	// Save the current filename, if any, and display it, along with the
	// current mode.
	m_fileName = fn;
	displayFileName();

	// If there is no filename, force a selection.
	if(m_fileName == "") {
		chooseFile();
	}

	QObject::connect(this, &Page_3::setSide, m_cl->sk, &skin::setSide);
}

void
Page_3::displayFileName()
{
	QString cur = "Current File: ";

	if(m_fileName == "") {
		cur += "(no file)";
		ui->newFile->setText("Choose file");
	} else {
		cur += m_fileName;
		ui->newFile->setText("Change file");
	}
	ui->currentFile->setText(cur);
}

void
Page_3::chooseFile()
{
	QSettings settings;
	const QString DEFAULT_DIR_KEY("default_dir");

	QStringList fnList = {};

	// Choose / create an appropriate file.
	QFileDialog fileDialog;
	fileDialog.setFileMode(QFileDialog::AnyFile);
	fileDialog.setDirectory(settings.value(DEFAULT_DIR_KEY, QDir::homePath()).toString());
	if(fileDialog.exec()) {
		fnList = fileDialog.selectedFiles();
	}

	if(fnList.isEmpty()) {
		return;
	}
	m_fileName = fnList.at(0);

	// Save the path to the selected file for next time.
	QFileInfo fi(m_fileName);
        settings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	//DEBUG_printf("file >>>%s<<<", qPrintable(m_fileName));
	displayFileName();
}

void
Page_3::side()
{
	QPushButton *button = qobject_cast<QPushButton *>(sender());
	QString s = button->objectName();

	if(s == "Side1") {
		emit setSide(1, m_fileName);
	} else {
		emit setSide(2, m_fileName);
	}

	close();
}

void
Page_3::closer()
{
	close();
}

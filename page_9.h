// HP67u Calculator Emulator
//
// Debugger->Microcode
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef PAGE_9_H
#define PAGE_9_H

#include <QDialog>

#include "global.h"

namespace Ui {
	class Page_9;
}

class Page_9 : public QDialog
{
	Q_OBJECT

	public:
		explicit Page_9(QWidget *parent = nullptr);
		~Page_9();

		void init(CLASSES *cl);

	public slots:

	private:
		Ui::Page_9 *ui;

		CLASSES *m_cl;

	private slots:
		void closer();
		void step();
		void faster();
		void slower();
		void turbo();
		void run();
		void stopLog();
		void log();
		void settings();

		void showRegisters(QString s);
		void refresh();
};

#endif // PAGE_9_H

// HP67u Calculator Emulator - magcard
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _MAGCARD_H_
#define _MAGCARD_H_

#include "global.h"

// I got the following from the November 1976 issue of the
// HP Journal.
//
// Each side of the card contains 952 bits.  The card reader has two 28-bit
// buffers, and "28 bits" is also the record size.  Thus, there are 952 / 28
// or 34 records per side.
//
// The first record contains info about that track, which can indicate one of
// the following track types:
//
// One-sided program
// First side of two-sided program
// Second side of two-sided program
// One-sided data file
// First side of two-sided data file
// Second side of two-sided data file
//
// There may be other info in that record.
//
// The next 32 records are either 122 program steps at 3.5 steps per record,
// or 16 data registers at 0.5 registers per record.  A register is 56 bits
// wide.
//
// The last record is the checksum of the data on the track.
//
// The javascript calculator packs each record into 7 characters, as ASCII
// hex, thus wasting half the space, but I guess that is easier than using
// a binary format.  There are 38 records on a side instead of the 34 that
// I'd expect.  Perhaps I'll figure out why there are 4 extras as I go
// through the code.
//
// Each javascript record has a CRLF separator, and C needs a null at the end
// of the buffer.

class magcard : public QThread
{
	Q_OBJECT

	public:
		magcard();
		~magcard();

		void		init(CLASSES *cl);

		void		clear();
		bool		fmString(QString s);
		QString		toString();
		QString		crdToTxt();

		std::array<double, DATA_REGS> getData();
		void		setData(std::array<double, DATA_REGS> a);

		std::array<uint8_t, CONTENT_LENGTH> getContent();
		void		setContent(std::array<uint8_t, CONTENT_LENGTH> array);

		QString		getTitle();
		void		setTitle(QString s);

		std::array<QString, MAX_LABELS> getLabels();
		void		setLabels(std::array<QString, MAX_LABELS> a);

		QString		getHelp();
		void		setHelp(QString s);

		SIDE_TYPE	getSide(int side);
		void		setSide(int side, SIDE_TYPE s);

		STATE_SETTINGS	getSettings();

	private:
		QString		substituteHTML(QString s);
		QString		getRHS(QString s, QString& lhs);
		int		keycode2progcode(QString s);
		bool		loadSection(
					QString name,
					int len,
					QString& s
					);

		QString		ato7(SIDE_TYPE a);
		QString		buildString(int format);

		int		m_lineNumber;

		uint8_t		m_content[CONTENT_LENGTH];

		bool		m_side1_loaded;
		SIDE_TYPE	m_side1;

		bool		m_side2_loaded;
		SIDE_TYPE	m_side2;

		STATE_SETTINGS	m_settings;

		QString		m_title;
		QString		m_help;
		std::array<QString, MAX_LABELS> m_labels;
		std::array<double, DATA_REGS> m_data;

		CLASSES		*m_cl;
};

#endif // _MAGCARD_H_

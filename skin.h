// HP67u Calculator Emulator - skin
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _SKIN_H_
#define _SKIN_H_

#include "global.h"

class skin : public QThread
{
	Q_OBJECT

	public:
		skin();
		~skin();

		void init(CLASSES *cl);

		void showPage(QString id);
		void skSetScaleAndCenterDiv(QString id, int w, int h);

		int sz(int n);

		void OnPage1Power();
		void OnPage1Wpgm();

		int getLogMode();
		void setLogMode(int i);

		void updDisplay(QString s);
		void updRegisters(std::array<QString, REG_LIST_SIZE> a);
		void updLog(std::array<QString, REG_LIST_SIZE> a);
		void updLogText(QString msg);
		void updCardRead(uint8_t *a);
		void updCardWrite(uint8_t *a);
		void updCardDisplay(bool on);

		void startup();

	public slots:
		void chooseCard();
		void setSide(int side, QString fn);

	signals:
		void cardImage(bool v);
		void cardTitle(QString s);
		void cardLabels(std::array<QString, MAX_LABELS> a);
		void sendDigit(QString s);
		void sendRegisters(QString s);
		void refreshPage9();
		void forcedResize();

	private:
		void openLog();
		void closeLog();

		QFile m_logFile;
		QTextStream m_skLog;

		QFile m_cardFile;

		bool m_skPower;
		bool m_skWpgm;

		int m_skScale;
		int m_skCardSide;
		int m_skLogMode;

		std::array<QString, REG_LIST_SIZE> m_skLogPrev;

		QString m_skCardName;

		CLASSES *m_cl;
};


#endif // _SKIN_H_

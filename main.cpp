// HP67u Calculator Emulator - main
//
// Copyright (c) 2023 Steven A. Falco
// 
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("HP67");
    app.setOrganizationName("Falco");
    app.setOrganizationDomain("falco.home.arpa");

    HP67 w;
    w.startup(&app);
    w.show();

    return app.exec();
}

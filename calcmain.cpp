// HP67u Calculator Emulator - calcmain
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "calcmain.h"

#include "aar.h"
#include "controlAndTiming.h"
#include "rom67asm.h"
#include "skin.h"

calcmain::calcmain()
{
	//DEBUG_printf("calcmain constructed");
}

calcmain::~calcmain()
{
	//DEBUG_printf("calcmain destroyed");
}

void
calcmain::init(CLASSES *cl)
{
	m_cl = cl;

	m_last_d = "(none)";
	m_calcKeyDown = false;
	m_turbo = false;
}

bool
calcmain::getCalcKeyDown()
{
	return m_calcKeyDown;
}

void
calcmain::setCalcKeyDown(bool n)
{
	m_calcKeyDown = n;
}

void
calcmain::doPowerOff()
{
	//DEBUG_printf("powering off");
	m_cl->cat->setPower(false);
	disp();
}

void
calcmain::doPowerOn()
{
	m_cl->aar->init(m_cl);
	m_cl->cat->init(m_cl);
	m_cl->cat->boot();
}

void
calcmain::doKey(uint8_t x)
{
	m_cl->cat->setkey(x);
	m_calcKeyDown= true;
}

void
calcmain::doMouseUp()
{
	m_calcKeyDown= false;

#if 0
	std::array<QString, REG_LIST_SIZE> q = _regs();
	for(int i = 0; i < REG_LIST_SIZE; i++) {
		DEBUG_printf("%s", qPrintable(q[i]));
	}
	DEBUG_printf("\n");
#endif
}

// custom

void
calcmain::doWpgmOff()
{
	m_cl->cat->switchPrgm(0);
}

void
calcmain::doWpgmOn()
{
	m_cl->cat->switchPrgm(1);
}

void
calcmain::doCardInsert()
{
	m_cl->aar->crc_start(); // crc_card_inserted = true;
	m_cl->cat->start();
}
 
// show 10ths
QString
calcmain::n1(double x)
{
	return QString("%1").arg(x, 0, 'f', 1);
}

void
calcmain::doStep()
{
	m_cl->cat->setStepsPerTick(1);
	m_cl->cat->setTicksPerSec(1);
	m_cl->cat->setTrace(true);
	m_cl->cat->setSingleStep(true);
       	m_speed = "Speed: SingleStep";
	m_cl->cat->manualStep();
}

void
calcmain::setSpeedRun()
{
	m_cl->cat->setStepsPerTick(80);
	m_cl->cat->setTicksPerSec(40);
	m_cl->cat->setTrace(false);
	m_cl->cat->setSingleStep(false);
	m_speed = "Speed: Normal";
       	m_cl->cat->start();
}

void
calcmain::setSpeedTurbo()
{
	m_cl->cat->setStepsPerTick(2000);
	m_cl->cat->setTicksPerSec(1000);
	m_cl->cat->setTrace(false);
	m_cl->cat->setSingleStep(false);
	m_speed = "Speed: Turbocharged!";
       	m_cl->cat->start();
}

void
calcmain::setSpeedFaster()
{
	m_cl->cat->setStepsPerTick(1);
	m_cl->cat->setTicksPerSec(m_cl->cat->getTicksPerSec() * (3.0 / 2.0));
	m_cl->cat->setTrace(true);
	m_cl->cat->setSingleStep(false);
	m_speed = "Speed: " + n1(m_cl->cat->getTicksPerSec()) + "/sec";
	m_cl->cat->start();
}

void
calcmain::setSpeedSlower()
{
	m_cl->cat->setStepsPerTick(1);
	m_cl->cat->setTicksPerSec(m_cl->cat->getTicksPerSec() * (2.0 / 3.0));
	m_cl->cat->setTrace(true);
	m_cl->cat->setSingleStep(false);
	m_speed = "Speed: " + n1(m_cl->cat->getTicksPerSec()) + "/sec";
	m_cl->cat->start();
}

QString
calcmain::getSpeed()
{
       	return m_speed;
}

bool
calcmain::isRunning()
{
	return (m_cl->cat->getTrace() == false);
}

// We are called with an array of 64 registers as strings.
void
calcmain::setRawData(std::array<QString, MAX_REGISTERS> a)
{
	int i, j;
	unsigned long v;

	char buf[SMALL_BUFFER];

	for(i = 0; i < MAX_REGISTERS; i++) {
		for(j = (MAX_RAM_COLS - 1); j >= 0; j--) {
			buf[0] = a[i][((MAX_RAM_COLS - 1) - j)].cell();
			buf[1] = 0;
			v = strtoul(buf, 0, 16);
			m_cl->aar->getRAM()->ram[i][j] = v;
		}
	}
}

std::array<QString, MAX_REGISTERS>
calcmain::getRawData()
{
	int i, j;

	std::array<QString, MAX_REGISTERS> a = {};

	for(i = 0; i < MAX_REGISTERS; i++) {
		a[i] = "";
		for(j = (MAX_RAM_COLS - 1); j >= 0; j--) {
			a[i] += charForDigit(m_cl->aar->getRAM()->ram[i][j]);
		}
	}

	return a;
}

std::array<QString, MAX_RAM_ROWS>
calcmain::getAllData()
{
	int i, j;

	std::array<QString, MAX_RAM_ROWS> a = {};

	for(i = 0; i < MAX_RAM_ROWS; i++) {
		a[i] = "";
		for(j = (MAX_RAM_COLS - 1); j >= 0; j--) {
			a[i] += charForDigit(m_cl->aar->getRAM()->ram[i][j]);
		}
	}

	return a;
}

QString
calcmain::getD_PC()
{
	bool displayMode = m_cl->aar->getDisplay14Digit();
	bool displayState = m_cl->cat->getDisplay();

	int pc = m_cl->aar->getPC();
	int bank = m_cl->aar->getBank();
	int addr = (bank * 4096) + pc;

	char buf[SMALL_BUFFER];

	QString s = "";

	snprintf(buf, SMALL_BUFFER, "display: %d/%s\n", displayMode ? 15 : 12, displayState ? "On" : "Off");
	s += buf;

	snprintf(buf, SMALL_BUFFER, "pc: %05o\n", addr);
	s += buf;

	return s;
}

double
calcmain::cm_bc2d(std::array<uint8_t, MAX_RAM_COLS> *r)
{
	QString dstr = "";
	int i;

	dstr += ((*r)[13] >= 8 ? "-" : "");
	for(i = 12; i >= 3; i--) {
		dstr += std::to_string((*r)[i]);
	}

	double mant = dstr.toDouble() / 1000000000.0;

	double exp = ((*r)[1] * 10) + (*r)[0];

	if((*r)[2] >= 8) {
		exp -= 100; // 998 -> -02
	}

	return mant * pow(10, exp);
}

std::array<uint8_t, MAX_RAM_COLS>
calcmain::cm_d2bc(double v)
{
	std::array<uint8_t, MAX_RAM_COLS> a;

	int i;
	int exponent;

	char buf[SMALL_BUFFER];
	char *p;

	// Format the number.
	snprintf(buf, SMALL_BUFFER, "%+.9e", v);
	p = buf;

	// Mantissa sign.
	if(*p++ == '-') {
		a[13] = 9;
	} else {
		a[13] = 0;
	}

	// Store the mantissa digits.
	for(i = 12; i >= 3; i--) {
		if(*p == '.') {
			// Skip over the decimal point.
			p++;
		}
		a[i] = *p++ - '0';
	}

	++p; // Skip over the 'e'

	// Exponent sign.
	if(*p++ == '-') {
		a[2] = 9;
	} else {
		a[2] = 0;
	}

	// The exponent is wacky, because it has a bias of 100 for negative
	// values.  First, convert it to an integer.
	exponent = ((p[0] - '0') * 10) + (p[1] - '0');
	
	// Correct it if negative.
	if(a[2] >= 8) {
		exponent = 100 - exponent;
	}

	// Store it.
	a[1] = exponent / 10;
	a[0] = exponent % 10;

	return a;
}

std::array<double, STACK_ELEMENTS>
calcmain::getStack()
{
	std::array<double, STACK_ELEMENTS> stk;
	std::array<uint8_t, MAX_RAM_COLS> v;

	v = m_cl->aar->getRegM2();
	stk[0] = cm_round(cm_bc2d(&v));	// LST x

	v = m_cl->aar->getRegC();
	stk[1] = cm_round(cm_bc2d(&v));	// cm_getx(a,b);

	v = m_cl->aar->getRegD();
	stk[2] = cm_round(cm_bc2d(&v));

	v = m_cl->aar->getRegE();
	stk[3] = cm_round(cm_bc2d(&v));

	v = m_cl->aar->getRegF();
	stk[4] = cm_round(cm_bc2d(&v));

	return stk;
}

std::array<double, DATA_REGS>
calcmain::getData()
{
	int i, j, k;

	std::array<uint8_t, MAX_RAM_COLS> v;
	std::array<double, DATA_REGS> a;

	i = 0;

	for(j = 0; j < 10; i++, j++) {
		// 00-09: mems  0 - 9 are in a[0 - 9]
		for(k = 0; k < MAX_RAM_COLS; k++) {
			v[k] = m_cl->aar->getRAM()->ram[0 + j][k];
		}
		a[i] = cm_bc2d(&v);
	}

	for(j = 0; j < 10; i++, j++) {
		// 10-19: mems s0-s9 are in a[48-57]
		for(k = 0; k < MAX_RAM_COLS; k++) {
			v[k] = m_cl->aar->getRAM()->ram[48 + j][k];
		}
		a[i] = cm_bc2d(&v);
	}

	for(j = 0; j <  6; i++, j++) {
		// 20-25: mems A-E,I are in a[10-15]
		for(k = 0; k < MAX_RAM_COLS; k++) {
			v[k] = m_cl->aar->getRAM()->ram[10 + j][k];
		}
		a[i] = cm_bc2d(&v);
	}

	return a;
}

void
calcmain::setData(std::array<double, DATA_REGS> data)
{
	int i, j, k;

	std::array<uint8_t, MAX_RAM_COLS> v;

	i = 0;

	for(j = 0; j < 10; i++, j++) {
		// 00-09: mems  0 - 9 are in a[0 - 9]
		v = cm_d2bc(data[i]);
		for(k = 0; k < MAX_RAM_COLS; k++) {
			m_cl->aar->getRAM()->ram[0 + j][k] = v[k];
		}
	}

	for(j = 0; j < 10; i++, j++) {
		// 10-19: mems s0-s9 are in a[48-57]
		v = cm_d2bc(data[i]);
		for(k = 0; k < MAX_RAM_COLS; k++) {
			v[k] = m_cl->aar->getRAM()->ram[48 + j][k] = v[k];
		}
	}

	for(j = 0; j <  6; i++, j++) {
		// 20-25: mems A-E,I are in a[10-15]
		v = cm_d2bc(data[i]);
		for(k = 0; k < MAX_RAM_COLS; k++) {
			m_cl->aar->getRAM()->ram[10 + j][k] = v[k];
		}
	}
}

// This returns an array of 224 bytes, corresponding to the current program
// in RAM. 
std::array<uint8_t, CONTENT_LENGTH>
calcmain::getProg()
{
	std::array<uint8_t, CONTENT_LENGTH> a = {};
	RAM *ram = m_cl->aar->getRAM();

	int i, j, k;

	k = 0;
	for(i = 47; i >= 16; i--) {
		for(j = 0; j < MAX_RAM_COLS; j += 2) {
			a[k++] = (ram->ram[i][j + 1] << 4) | ram->ram[i][j];
		}
	}

	return a;
}

void
calcmain::setProg(std::array<uint8_t, CONTENT_LENGTH> a)
{
	int i, j, k;
	RAM *ram = m_cl->aar->getRAM();

#if 0
	for(i = 0; i < CONTENT_LENGTH; i++) {
		if(i % 16 == 0) {
			fprintf(stderr, "%3d:", i);
		}
		fprintf(stderr, " %02x", a[i]);
		if(i % 16 == 15) {
			fprintf(stderr, "\n");
		}
	}
#endif

	k = 0;
	for(i = 47; i >= 16; i--) {
		for(j = 0; (k < CONTENT_LENGTH) && (j < MAX_RAM_COLS); j += 2) {
			ram->ram[i][j + 0] = a[k] & 0x0f;
			ram->ram[i][j + 1] = a[k] >> 4;
			k++;
		}
	}

	// if you load any program via setProg()
	// execute a "test/clear default functions" instruction
	m_cl->aar->op_crc(1100);
}

void
calcmain::setSettings(STATE_SETTINGS settings)
{
	if(!settings.valid) {
		return;
	}

	switch(settings.angleMode) {
		case 0: // Degrees - clear m_s[0] and m_s[14];
			m_cl->aar->setRegStatus(0, 0);
			m_cl->aar->setRegStatus(14, 0);
			break;

		case 1: // Radians - set m_s[0], clear m_s[14]
			m_cl->aar->setRegStatus(0, 1);
			m_cl->aar->setRegStatus(14, 0);
			break;

		case 2: // Grads - clear m_s[0], set m_s[14]
			m_cl->aar->setRegStatus(0, 0);
			m_cl->aar->setRegStatus(14, 1);
			break;
	}

	RAM *ram = m_cl->aar->getRAM();

	// 0x00 = Scientific mode
	// 0x22 = Fixed point mode
	// 0x40 = Engineering mode
	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_MODE_HI] = settings.displayMode >> 4;
	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_MODE_LO] = settings.displayMode & 0x0f;

	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_PLACES] = settings.displayPlaces;

	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_0] = (settings.flags & 0x08) ? 1 : 0;
	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_1] = (settings.flags & 0x04) ? 1 : 0;
	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_2] = (settings.flags & 0x02) ? 1 : 0;
	ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_3] = (settings.flags & 0x01) ? 1 : 0;

	// THIS IS A HACK, but it is the best I've gotten so far.  We have
	// poked values into RAM location RAM_SETTINGS_WORD but reg A and
	// reg B have not been updated because the fw doesn't see that yet.
	// I can fake it, by forcing the key sequence "h SPACE", which is a
	// no-op on an HP67.
	//
	// I need a small delay between the keystrokes to allow the fw to
	// react.  Of course, who knows how long those delays should be?
	// Probably depends on the host's cpu speed, and whether it is
	// multithreaded.  On my machine, as little as 1 us is sufficient,
	// so I've set the delay to 1 ms, which I hope will be safe.
	doKey(0x10);
	usleep(1000);
	doMouseUp();
	usleep(1000);
	doKey(0x70);
	usleep(1000);
	doMouseUp();
	//bool program = m_cl->cat->getProgram();
	//m_cl->cat->switchPrgm(!program);
	//usleep(1000);
	//m_cl->cat->switchPrgm(program);
}

// used when displaying registers and stack
double
calcmain::cm_round(double x)
{
    if(x == 0) {
	    return 0;
    }

    double n = 10;
    double d = ceil(M_LOG10E * log(x < 0 ? -x : x));
    double exponent = n - d;
    double magnitude = pow(10, exponent);
    double shifted = round(x * magnitude);

    return shifted / magnitude;
}

// x is not normalized. need to use a and b to determine value.
QString
calcmain::cm_getdisplay(
		std::array<uint8_t, MAX_RAM_COLS> a,
	       	std::array<uint8_t, MAX_RAM_COLS> b
		)
{
	QString dstr = "";

	int i;

	// mantissa
	dstr += (a[2] == 0 || a[2] == 2 || a[2] == 8) ? "-" : " ";
	for(i = 1; i < 12; i++) {
		if(b[MAX_RAM_COLS - i] == 0) {
			dstr += charForDigit(a[MAX_RAM_COLS - i]);
		} else if(b[MAX_RAM_COLS - i] == 3) {
			dstr += ".";
		} else {
			dstr += " ";
		}
	}

	// exponent
	dstr += (a[2] == 2 || a[2] == 8 || a[2] == 3 || a[2] == 9) ? "-" : " ";
	dstr += (b[1] == 0 || b[1] == 4) ? charForDigit(a[1]) : " ";
	dstr += (b[0] == 0             ) ? charForDigit(a[0]) : " ";

	return dstr;
}

void
calcmain::disp()
{
	QString d = "               ";	// 15 blanks

#if 0
	DEBUG_printf("power %s display %s",
			m_cl->cat->getPower() ? "true" : "false",
			m_cl->cat->getDisplay() ? "true" : "false");
#endif
	if(m_cl->cat->getPower() && m_cl->cat->getDisplay()) {
		// if in wpgm then no "." so need extra " "
#if 0
		DEBUG_printf("a %s b %s",
				qPrintable(m_cl->aar->regToString(m_cl->aar->getRegA())),
				qPrintable(m_cl->aar->regToString(m_cl->aar->getRegB())));
#endif
		d = cm_getdisplay(m_cl->aar->getRegA(), m_cl->aar->getRegB()) + " ";
	}

	if(d != m_last_d) {
		//DEBUG_printf("display >>>%s<<<", qPrintable(d));
		m_cl->sk->updDisplay(d);
	}

	m_last_d = d;
}

#if 0
function cm_getx(a,b) {
	var dstr = cm_getdisplay(a,b);
	var x = (dstr.substr(0,11)*1); 
	if (b[2]<8) 
		x= x*Math.pow(10,dstr.substr(-3,3));
	return x;
}
#endif
   
QString
calcmain::printreg(std::array<uint8_t, MAX_RAM_COLS> r)
{
	QString p = "";

	int j;
	uint8_t nibble;
	char c;

	for(j = 0; j < MAX_RAM_COLS; j++) {
		nibble = r[j] & 0x0f;
		if(nibble <= 9) {
			c = '0' + nibble;
		} else {
			c = 'a' + (nibble - 10);
		}
		p = c + p;
	}

	return p;
}

std::array<QString, REG_LIST_SIZE>
calcmain::_regs()
{
	std::array<QString, REG_LIST_SIZE> q = {};

	QString str = "";

	int j;

	uint8_t nibble;
	char c;

	char buf[SMALL_BUFFER];

	uint32_t bank = m_cl->aar->getBank();
	uint32_t pc = m_cl->aar->getPC();
	uint32_t addr = (bank * 4096) + pc;

	q[ 0] = QString::number(m_cl->aar->getP());

	q[ 1] = printreg(m_cl->aar->getRegA());
	q[ 2] = printreg(m_cl->aar->getRegB());
	q[ 3] = printreg(m_cl->aar->getRegC());

	for(j = 0; j < MEM_STATUS_SZ; j++) {
		nibble = m_cl->aar->getRegStatus(j) & 0x0f;
		if(nibble <= 9) {
			c = '0' + nibble;
		} else {
			c = 'a' + (nibble - 10);
		}
		str += (nibble ? c : '.');
	}
	q[ 4] = str;

	snprintf(buf, SMALL_BUFFER, "%05o", addr);
	q[ 5] = buf;

	q[ 6] = m_cl->rasm->getInstruction(addr); 

	q[ 7] = "";

	q[ 8] = printreg(m_cl->aar->getRegD());
	q[ 9] = printreg(m_cl->aar->getRegE());
	q[10] = printreg(m_cl->aar->getRegF());
	q[11] = printreg(m_cl->aar->getRegM1());
	q[12] = printreg(m_cl->aar->getRegM2());

	return q;
}


// called by the skin to get the latest
void
calcmain::doShowMCRegs()
{
	std::array<QString, REG_LIST_SIZE> q = _regs();
	m_cl->sk->updRegisters(q);
}

// called by the calc to pass on the latest
void
calcmain::regs()
{
	std::array<QString, REG_LIST_SIZE> q = _regs();
	m_cl->sk->updRegisters(q);
	m_cl->sk->updLog(q);
}

void
calcmain::sayDbg(QString msg)
{
	m_cl->sk->updLogText(msg);
}

void
calcmain::cardRead(uint8_t *dest)
{
	m_cl->sk->updCardRead(dest);
}

void
calcmain::cardWrite(uint8_t *src)
{
	m_cl->sk->updCardWrite(src);
}

void
calcmain::cmCardDisplay(bool on)
{
	//DEBUG_printf("turn %s", on ? "on" : "off");
	m_cl->sk->updCardDisplay(on);
}

void
calcmain::calcStartup()
{
	m_speed = "Speed: Normal";
}

QString
calcmain::charForDigit(uint8_t n)
{
	QString s = "0123456789abcdef";

	return s[n];
}


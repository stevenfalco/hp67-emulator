// HP67u Calculator Emulator - magcard
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "magcard.h"

#include "aar.h"

// 99 nn, nn is decimal for undoc'ed prog code (on '67 use 99 02 55 if 255 is a rsvd code) 
static const char *keycodes[] = {
	"84"      , "35 62"   , "32 54"   , "31 54"   , "31 82"   , "21"      , "35 63"   , "31 52"   ,
	"32 52"   , "32 72"   , "31 62"   , "31 63"   , "31 64"   , "31 72"   , "35 22"   , "34 21"   ,
	"00"      , "01"      , "02"      , "03"      , "04"      , "05"      , "06"      , "07"      ,
	"08"      , "09"      , "83"      , "41"      , "42"      , "43"      , "81"      , "99 00 31",
	"35 72"   , "35 81"   , "31 21"   , "32 21"   , "32 82"   , "35 21"   , "35 64"   , "31 53"   ,
	"32 53"   , "31 83"   , "32 62"   , "32 63"   , "32 64"   , "32 83"   , "31 24"   , "99 00 47",
	"35 52"   , "35 53"   , "44"      , "35 23"   , "31 23"   , "31 84"   , "32 23"   , "61"      ,
	"51"      , "71"      , "32 73"   , "31 73"   , "32 74"   , "31 74"   , "33 24"   , "34 24"   ,
	"35 83"   , "35 84"   , "32 84"   , "35 82"   , "31 41"   , "32 41"   , "35 24"   , "35 54"   ,
	"35 73"   , "35 41"   , "35 42"   , "35 43"   , "31 42"   , "31 43"   , "35 74"   , "99 00 79",
	"32 61"   , "32 51"   , "32 81"   , "31 61"   , "31 51"   , "31 81"   , "31 71"   , "32 71"   ,
	"35 71 00", "35 71 01", "35 71 02", "35 71 03", "31 34"   , "32 34"   , "31 33"   , "32 33"   ,
	"23 00"   , "23 01"   , "23 02"   , "23 03"   , "23 04"   , "23 05"   , "23 06"   , "23 07"   ,
	"23 08"   , "23 09"   , "35 61 00", "35 61 01", "35 61 02", "35 61 03", "99 01 10", "23 24"   ,
	"34 00"   , "34 01"   , "34 02"   , "34 03"   , "34 04"   , "34 05"   , "34 06"   , "34 07"   ,
	"34 08"   , "34 09"   , "34 11"   , "34 12"   , "34 13"   , "34 14"   , "34 15"   , "35 34"   ,
	"33 81 00", "33 81 01", "33 81 02", "33 81 03", "33 81 04", "33 81 05", "33 81 06", "33 81 07",
	"33 81 08", "33 81 09", "35 51 00", "35 51 01", "35 51 02", "35 51 03", "99 01 42", "33 81 24",
	"33 00"   , "33 01"   , "33 02"   , "33 03"   , "33 04"   , "33 05"   , "33 06"   , "33 07"   ,
	"33 08"   , "33 09"   , "33 11"   , "33 12"   , "33 13"   , "33 14"   , "33 15"   , "35 33"   ,
	"33 51 00", "33 51 01", "33 51 02", "33 51 03", "33 51 04", "33 51 05", "33 51 06", "33 51 07",
	"33 51 08", "33 51 09", "32 22 11", "32 22 12", "32 22 13", "32 22 14", "32 22 15", "33 51 24",
	"31 22 00", "31 22 01", "31 22 02", "31 22 03", "31 22 04", "31 22 05", "31 22 06", "31 22 07",
	"31 22 08", "31 22 09", "31 22 11", "31 22 12", "31 22 13", "31 22 14", "31 22 15", "31 22 24",
	"33 61 00", "33 61 01", "33 61 02", "33 61 03", "33 61 04", "33 61 05", "33 61 06", "33 61 07",
	"33 61 08", "33 61 09", "22 31 11", "22 31 12", "22 31 13", "22 31 14", "22 31 15", "33 61 24",
	"22 00"   , "22 01"   , "22 02"   , "22 03"   , "22 04"   , "22 05"   , "22 06"   , "22 07"   ,
	"22 08"   , "22 09"   , "22 11"   , "22 12"   , "22 13"   , "22 14"   , "22 15"   , "22 24"   ,
	"33 71 00", "33 71 01", "33 71 02", "33 71 03", "33 71 04", "33 71 05", "33 71 06", "33 71 07",
	"33 71 08", "33 71 09", "32 25 11", "32 25 12", "32 25 13", "32 25 14", "32 25 15", "33 71 24",
	"31 25 00", "31 25 01", "31 25 02", "31 25 03", "31 25 04", "31 25 05", "31 25 06", "31 25 07",
	"31 25 08", "31 25 09", "31 25 11", "31 25 12", "31 25 13", "31 25 14", "31 25 15", "99 02 55",
};

static int keycodesLength = sizeof(keycodes) / sizeof(char *); // 256 entries

static const char *keyText[] = {
      "R/S"      , "h 1/x"    , "g x^2"    , "f sqrt"   , "f %"      , "SUM+"     , "h y^x"    , "f LN"     ,
      "g e^x"    , "g R->P"   , "f SIN"    , "f COS"    , "f TAN"    , "f R<-P"   , "h RTN"    , "RCL SUM"  ,
      "0"        , "1"        , "2"        , "3"        , "4"        , "5"        , "6"        , "7"        ,
      "8"        , "9"        , "."        , "ENTER"    , "CHS"      , "EEX"      , "/"        , "(1F)"     ,
      "h PAUSE"  , "h N!"     , "f mean"   , "g sdev"   , "g %ch"    , "h SUM-"   , "h ABS"    , "f LOG"    ,
      "g 10^x"   , "f INT"    , "g SIN-1"  , "g COS-1"  , "g TAN-1"  , "g FRAC"   , "f RND"    , "(2F)"     ,
      "h x<->y"  , "h Rv"     , "CLx"      , "h ENG"    , "f FIX"    , "f -x-"    , "g SCI"    , "+"        ,
      "-"        , "x"        , "g D->R"   , "f D<-R"   , "g ->H.MS" , "f <-H.MS" , "STO (i)"  , "RCL (i)"  ,
      "h H.MS+"  , "h SPACE"  , "g STK"    , "h LSTx"   , "f W/DATA" , "g MERGE"  , "h x<->I"  , "h R^"     ,
      "h pi"     , "h DEG"    , "h RAD"    , "h GRD"    , "f P<->S"  , "f CLREG"  , "h REG"    , "(4F)"     ,
      "g x<>y"   , "g x=y"    , "g x>y"    , "f x<>0"   , "f x=0"    , "f x>0"    , "f x<0"    , "g x<=y"   ,
      "h F? 0"   , "h F? 1"   , "h F? 2"   , "h F? 3"   , "f ISZ"    , "g ISZ(i)" , "f DSZ"    , "g DSZ(i)" ,
      "DSP 0"    , "DSP 1"    , "DSP 2"    , "DSP 3"    , "DSP 4"    , "DSP 5"    , "DSP 6"    , "DSP 7"    ,
      "DSP 8"    , "DSP 9"    , "h CF 0"   , "h CF 1"   , "h CF 2"   , "h CF 3"   , "(6E)"     , "DSP (i)"  ,
      "RCL 0"    , "RCL 1"    , "RCL 2"    , "RCL 3"    , "RCL 4"    , "RCL 5"    , "RCL 6"    , "RCL 7"    ,
      "RCL 8"    , "RCL 9"    , "RCL A"    , "RCL B"    , "RCL C"    , "RCL D"    , "RCL E"    , "h RCI"    ,
      "STO / 0"  , "STO / 1"  , "STO / 2"  , "STO / 3"  , "STO / 4"  , "STO / 5"  , "STO / 6"  , "STO / 7"  ,
      "STO / 8"  , "STO / 9"  , "h SF 0"   , "h SF 1"   , "h SF 2"   , "h SF 3"   , "(8E)"     , "STO / (i)",
      "STO 0"    , "STO 1"    , "STO 2"    , "STO 3"    , "STO 4"    , "STO 5"    , "STO 6"    , "STO 7"    ,
      "STO 8"    , "STO 9"    , "STO A"    , "STO B"    , "STO C"    , "STO D"    , "STO E"    , "h STI"    ,
      "STO - 0"  , "STO - 1"  , "STO - 2"  , "STO - 3"  , "STO - 4"  , "STO - 5"  , "STO - 6"  , "STO - 7"  ,
      "STO - 8"  , "STO - 9"  , "g GSBf a" , "g GSBf b" , "g GSBf c" , "g GSBf d" , "g GSBf e" , "STO - (i)",
      "f GSB 0"  , "f GSB 1"  , "f GSB 2"  , "f GSB 3"  , "f GSB 4"  , "f GSB 5"  , "f GSB 6"  , "f GSB 7"  ,
      "f GSB 8"  , "f GSB 9"  , "f GSB A"  , "f GSB B"  , "f GSB C"  , "f GSB D"  , "f GSB E"  , "f GSB (i)",
      "STO + 0"  , "STO + 1"  , "STO + 2"  , "STO + 3"  , "STO + 4"  , "STO + 5"  , "STO + 6"  , "STO + 7"  ,
      "STO + 8"  , "STO + 9"  , "GTO f a"  , "GTO f b"  , "GTO f c"  , "GTO f d"  , "GTO f e"  , "STO + (i)",
      "GTO 0"    , "GTO 1"    , "GTO 2"    , "GTO 3"    , "GTO 4"    , "GTO 5"    , "GTO 6"    , "GTO 7"    ,
      "GTO 8"    , "GTO 9"    , "GTO A"    , "GTO B"    , "GTO C"    , "GTO D"    , "GTO E"    , "GTO (i)"  ,
      "STO x 0"  , "STO x 1"  , "STO x 2"  , "STO x 3"  , "STO x 4"  , "STO x 5"  , "STO x 6"  , "STO x 7"  ,
      "STO x 8"  , "STO x 9"  , "g LBLf a" , "g LBLf b" , "g LBLf c" , "g LBLf d" , "g LBLf e" , "STO x (i)",
      "f LBL 0"  , "f LBL 1"  , "f LBL 2"  , "f LBL 3"  , "f LBL 4"  , "f LBL 5"  , "f LBL 6"  , "f LBL 7"  ,
      "f LBL 8"  , "f LBL 9"  , "f LBL A"  , "f LBL B"  , "f LBL C"  , "f LBL D"  , "f LBL E"  , "(FF)"     ,
};

static int keyTextLength = sizeof(keyText) / sizeof(char *); // 256 entries

// Public functions.

// Initialize all the field of the magcard.
magcard::magcard()
{
	clear();

	//DEBUG_printf("magcard constructed");
}

magcard::~magcard()
{
	//DEBUG_printf("magcard destroyed");
}

void
magcard::init(CLASSES *cl)
{
	m_cl = cl;
}

void
magcard::clear()
{
	int i;

	m_lineNumber = 0;

	for(i = 0; i < CONTENT_LENGTH; i++) {
		m_content[i] = 0;
	}

	m_side1_loaded = false;
	m_side2_loaded = false;
	for(i = 0; i < CRC_RAWDATA_SZ; i++) {
		m_side1[i] = 0;
		m_side2[i] = 0;
	}

	m_settings.valid = false;

	m_title = "";
	m_help = "";

	for(i = 0; i < MAX_LABELS; i++) {
		m_labels[i] = "";
	}

	for(i = 0; i < DATA_REGS; i++) {
		m_data[i] = 0;
	}
}

// Build a very long string containing all the data for a card.
// This is used when we select the Internals->Program menu item.
QString
magcard::toString()
{
	return buildString(0);
}

// This is used when saving a card to disk in magcard format.
// This is a packed representation, rather than a list of keystrokes.
QString
magcard::crdToTxt()
{
	return buildString(1);
}

// Private functions.

// If format is 0, write content in PROG format.
// If format is 1, write content in SIDE1/SIDE2 format.
QString
magcard::buildString(int format)
{
	QString s = "";
	char sb[SMALL_BUFFER];

	QString nl = "\n";

	int i, j;

	if(format == 0) {
		// PROG data.
		s += "PROG" + nl;
		s += QString::number(CONTENT_LENGTH) + nl;

		// Each keycode is from 2 to 8 bytes long.  To each entry
		// we add 5 bytes in front for the address, and 2 bytes
		// at the end for the cr/lf, so each line is 15 bytes or less
		// in length.
		for(i = 0; i < CONTENT_LENGTH; i++) {
			snprintf(sb, SMALL_BUFFER, "%03d: %-8s : %s", i + 1, keycodes[m_content[i]], keyText[m_content[i]]);
			s += sb + nl;
		}
	} else {
		// SIDE1 data.
		s += "SIDE1" + nl;

		if(m_side1_loaded) {
			s += QString::number(RECORDS_PER_TRACK) + nl;
			s += ato7(m_side1);
		} else {
			s += QString::number(0) + nl;
		}

		// SIDE2 data.
		s += "SIDE2" + nl;

		if(m_side2_loaded) {
			s += QString::number(RECORDS_PER_TRACK) + nl;
			s += ato7(m_side2);
		} else {
			s += QString::number(0) + nl;
		}
	}

	// CARD title and labels.
	s += "CARD" + nl;
	s += QString::number(1 + MAX_LABELS) + nl;

	s += "Title: " + m_title + nl;

	QString labels[MAX_LABELS] = { "A", "B", "C", "D", "E", "a", "b", "c", "d", "e" };
	for(i = 0; i < MAX_LABELS; i++) {
		s += labels[i] + ": " + m_labels[i] + nl;
	}

	// DATA section
	s += "DATA" + nl;
	s += QString::number(DATA_REGS) + nl;
	for(i = 0; i < DATA_REGS; i++) {
		snprintf(sb, SMALL_BUFFER, "%.9e", m_data[i]);
		s += QString::number(i) + ": " + sb + nl;
	}
	
	// STATE section
	//
	// RAM word 26 stores some of the data that we need here.  But, it
	// is not easy to force the data back in when loading a card, because
	// the firmware apparently resets it on us.  I haven't solved that
	// yet, but we can at least put the right info in here.
	//
	// Call the nibbles of that word 0 through 13 in array order; i.e.
	// we call ram26[0] nibble 0.  Then the various nibbles that I've
	// decoded are:
	//
	// 7:   Number of places displayed (0-9)
	// 8+9: Display mode, 22 = Fixed, 00 = Sci, 40 = Eng
	// 10:  Flag 0
	// 11:  Flag 1
	// 12:  Flag 2
	// 13:  Flag 3
	//
	// The degree / radian / grad mode is stored in bits of the status
	// register.  I actually store the bits as an array of 16 bytes
	// because that is how the original javascript code did things.
	// Here are the bits:
	//
	// 0:   Set in Radian mode, cleared in Degree and Grad modes
	// 14:  Set in Grad mode, cleared in Degree mode
	s += "STATE" + nl;
	s += QString::number(STATE_LENGTH) + nl;

	uint8_t b0 = m_cl->aar->getRegStatus(0);
	uint8_t b14 = m_cl->aar->getRegStatus(14);
	if(b0) {
		s += "RAD\n";
	} else if(b14) {
		s += "GRD\n";
	} else {
		s += "DEG\n";
	}

	RAM *ram = m_cl->aar->getRAM();
	uint8_t modeHi = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_MODE_HI];
	uint8_t modeLo = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_MODE_LO];
	switch((modeHi << 4) | modeLo) {
		case 0x00:	s += "SCI\n"; break;
		case 0x22:	s += "FIX\n"; break;
		case 0x40:	s += "ENG\n"; break;
		default:	s += "???\n"; break;
	}

	uint8_t places = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_PLACES];
	s += QString::number(places) + nl;

	uint8_t flag0 = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_0];
	uint8_t flag1 = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_1];
	uint8_t flag2 = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_2];
	uint8_t flag3 = ram->ram[RAM_SETTINGS_WORD][RAM_SETTINGS_FLAG_3];
	s += flag0 ? "1\n" : "0\n";
	s += flag1 ? "1\n" : "0\n";
	s += flag2 ? "1\n" : "0\n";
	s += flag3 ? "1\n" : "0\n";
	
	// HELP section.  Note the this section has embedded newlines.
	s += "HELP" + nl;

	// Count the number of lines of help and write that number.
	j = m_help.count(nl);
	s += QString::number(j) + nl;
	//DEBUG_printf("number of help lines %d", j);

	// Write the actual help text.  This should have all the
	// nl bytes already, so we don't need to append them.
	s += m_help;

	// Write an endmarker.
	s += "END" + nl;

	return s;
}

// Look up a sequence of keystrokes and turn it into an integer representing
// the corresponding program code.
//
// Simple linear search.
int
magcard::keycode2progcode(QString s)
{
	int i;

	for(i = 0; i < keycodesLength; i++) {
		if(s == keycodes[i]) {
			return i;
		}
	}

	return 0; // Index 0 is the R/S key, which is essentially a NOP here.
}

QString
magcard::substituteHTML(QString s)
{
	// Replace the html junk.  I don't cover everything as unicode is too
	// large. :-)
	//
	// But these are the ones in the example cards that I've found so far.
	// Much more can be added as needed.

	s.replace("&lArr;",	"\u2190", Qt::CaseInsensitive);
	s.replace("&uArr;",	"\u2191", Qt::CaseInsensitive);
	s.replace("&rArr;",	"\u2192", Qt::CaseInsensitive);
	s.replace("&dArr;",	"\u2193", Qt::CaseInsensitive);

	s.replace("&delta;",	"\u0394", Qt::CaseInsensitive);
	s.replace("&divide;",	"\u00f7", Qt::CaseInsensitive);

	return s;
}

// Some strings use colons ':' to separate fields.  This routine
// returns the first and second field of a string.
QString
magcard::getRHS(QString s, QString& lhs)
{
	lhs = substituteHTML(s.section(':', 0, 0).trimmed());
	return substituteHTML(s.section(':', 1, 1).trimmed());
}

// We are given a very long string with embedded newlines.  The basic format
// is a tag, like "PROG", followed with a length, like "38", followed by
// that number of items.  Then, we will get the next tag, and so on, until
// we hit the "END" keyword.
//
// As we parse each section, we call loadSection to put it into card memory.
bool
magcard::fmString(QString s)
{
	qsizetype pos = 0;

	int section_length;

	QString section;
	QString length;
	QString nl = "\n";

	// Remove carriage returns; we just want line feeds.
	s.remove('\r');

	m_lineNumber = 0;
	while(1) {
		// Locate the next token.   This is the section name.
		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		// Extract the token and delete it from the source string.
		section = s.mid(0, pos);
		//DEBUG_printf("Section %s", qPrintable(section));
		if(section == "END") {
			return true;
		}
		s.remove(0, pos + nl.length());

		// Locate the next token.   This is the section length.
		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		// Extract the token and delete it from the source string.
		length = s.mid(0, pos);
		section_length = length.toInt();
		//DEBUG_printf("Section length %d", section_length);
		s.remove(0, pos + nl.length());

		// Load the section.  Note that loadSection takes a reference
		// to the string and will modify our copy.
		if(!loadSection(section, section_length, s)) {
			// Malformed input.
			return false;
		}
	}
}

// Note - we receive "s" as a reference, and we will modify it.  Our caller
// will see those modifications.
bool
magcard::loadSection(QString name, int len, QString& s)
{
	int i, j;
	unsigned long value;
	uint8_t nibble;

	qsizetype pos = 0;

	QString line;
	QString nl = "\n";
	QString lhs;

	if(name == "PROG") {
		if(len != CONTENT_LENGTH) {
			return false;
		}

		// Typically, there will be three fields in each
		// entry, delimited by ":".  The first is the step
		// number, like 001.  Next we have the sequence of
		// keycodes (one to three codes), and last we have 
		// an optional human-readable interpretation of the
		// keycodes.
		for(i = 0; i < CONTENT_LENGTH; i++) {
			// Find the next entry.
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			// Extract the entry and delete it from the
			// source string.
			line = s.mid(0, pos);
			s.remove(0, pos + nl.length());

			// Parse and store this entry.
			m_content[i] = keycode2progcode(getRHS(line, lhs));
			if(lhs.toInt() != (i + 1)) {
				return false;
			}
		}
	} else if(name == "SIDE1") {
		// magcard format is strings of 7 ASCII hex nibbles.
		if(len) {
			if(len != RECORDS_PER_TRACK) {
				return false;
			}

			m_side1_loaded = true;
			for(i = 0; i < RECORDS_PER_TRACK; i++) {
				// Find the next entry.
				if((pos = s.indexOf(nl)) == -1) {
					alert("Ran out of data at line %d", m_lineNumber);
					return false;
				}
				++m_lineNumber;

				// Extract the entry and delete it from the
				// source string.
				line = s.mid(0, pos);
				s.remove(0, pos + nl.length());

				// Parse and store the 7 nibbles of this entry.
				value = strtoul(qPrintable(line), 0, 16);
				for(j = 0; j < CHARS_PER_RECORD; j++) {
					nibble = (value >> (4 * ((CHARS_PER_RECORD - 1) - j))) & 0xf;
					m_side1[(CHARS_PER_RECORD * i) + j] = nibble;
				}
			}
		}
	} else if(name == "SIDE2") {
		// magcard format is strings of 7 ASCII hex nibbles.
		if(len) {
			if(len != RECORDS_PER_TRACK) {
				return false;
			}

			m_side2_loaded  = true;
			for(i = 0; i < RECORDS_PER_TRACK; i++) {
				// Find the next entry.
				if((pos = s.indexOf(nl)) == -1) {
					alert("Ran out of data at line %d", m_lineNumber);
					return false;
				}
				++m_lineNumber;

				// Extract the entry and delete it from the
				// source string.
				line = s.mid(0, pos);
				s.remove(0, pos + nl.length());

				// Parse and store the 7 nibbles of this entry.
				value = strtoul(qPrintable(line), 0, 16);
				for(j = 0; j < CHARS_PER_RECORD; j++) {
					nibble = (value >> (4 * ((CHARS_PER_RECORD - 1) - j))) & 0xf;
					m_side2[(CHARS_PER_RECORD * i) + j] = nibble;
				}
				++m_lineNumber;
			}
		}
	} else if(name == "CARD") {
		if(len != (1 + MAX_LABELS)) {
			return false;
		}

		// Store the title.
		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		// Extract the entry and delete it from the
		// source string.
		m_title = getRHS(s.mid(0, pos), lhs);
		s.remove(0, pos + nl.length());

		// Store the labels.
		--len;
		for(i = 0; i < len && i < MAX_LABELS; i++) {
			// Find a label.
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			m_labels[i] = getRHS(s.mid(0, pos), lhs);
			s.remove(0, pos + nl.length());
		}

	} else if(name == "STATE") {
		if(len != STATE_LENGTH) {
			return false;
		}

		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		line = s.mid(0, pos);
		s.remove(0, pos + nl.length());

		// Assume degree mode.
		m_settings.angleMode = 0;
		if(line == "RAD") {
			m_settings.angleMode = 1;
		} else if(line == "GRD") {
			m_settings.angleMode = 2;
		}
		
		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		line = s.mid(0, pos);
		s.remove(0, pos + nl.length());

		// Assume fixed mode.
		m_settings.displayMode = 0x22;
		if(line == "SCI") {
			m_settings.displayMode = 0x00;
		} else if(line == "ENG") {
			m_settings.displayMode = 0x40;
		}

		if((pos = s.indexOf(nl)) == -1) {
			alert("Ran out of data at line %d", m_lineNumber);
			return false;
		}
		++m_lineNumber;

		line = s.mid(0, pos);
		s.remove(0, pos + nl.length());

		m_settings.displayPlaces = line.toInt();

		// Flags.
		m_settings.flags = 0;
		j = 0x08;
		for(i = 0; i < FLAG_LENGTH; i++) {
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			line = s.mid(0, pos);
			s.remove(0, pos + nl.length());

			if(line == "1") {
				m_settings.flags |= j;
			}

			j >>= 1;
		}

		if(m_side1_loaded || m_side2_loaded) {
			// Mark our settings as invalid, because the microcode
			// will take care of loading them.  We don't want to
			// override that mechanism.
			m_settings.valid = false;
		} else {
			// Mark our settings as valid, since we didn't load
			// from a magcard, and hence the firmware won't know
			// the values.
			m_settings.valid = true;
		}
	} else if(name == "HELP") {
		m_help = "";

		for(i = 0; i < len; i++) {
			// Find the next newline.
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			//DEBUG_printf("pos %d, add >>%s<<", pos, qPrintable(s.mid(0, pos)));

			// Add it to m_help.
			m_help += s.mid(0, pos) + nl;
			s.remove(0, pos + nl.length());
		}
	} else if(name == "DATA") {
		if(len != DATA_REGS) {
			return false;
		}

		for(i = 0; i < DATA_REGS; i++) {
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			m_data[i] = getRHS(s.mid(0, pos), lhs).toDouble();
			s.remove(0, pos + nl.length());
		}
	} else {
		// Delete everything up to the next tag.
		for(i = 0; i < len; i++) {
			if((pos = s.indexOf(nl)) == -1) {
				alert("Ran out of data at line %d", m_lineNumber);
				return false;
			}
			++m_lineNumber;

			s.remove(0, pos + nl.length());
		}
		alert("unknown magcard section %s\n", qPrintable(name));
		return false;
	}

	return true;
}

// On entry, "a" is an array of bytes corresponding to the packed
// format used by the real HP magcards back in the day.  Each record
// is 7 hex bytes (really nibbles) from 0x00 to 0x0f, and there are
// 38 records on each side of the magcard.
//
// We basically pack the nibbles into a long string in groups of
// 7 characters, separated by newlines.
QString
magcard::ato7(SIDE_TYPE a)
{
	QString s = "";
	QString nl = "\n";

	uint8_t c;

	int i, j;

	for(i = 0; i < RECORDS_PER_TRACK; i++) {
		for(j = 0; j < CHARS_PER_RECORD; j++) {
			c = a[(i * CHARS_PER_RECORD) + j];
			if(c < 10) {
				s += QChar(char('0') + c);
			} else {
				s += QChar(char('a') + (c - 10));
			}
		}

		s += nl;
	}

	return s;
}

std::array<uint8_t, CONTENT_LENGTH>
magcard::getContent()
{
	std::array<uint8_t, CONTENT_LENGTH> array;

	int i;

	for(i = 0; i < CONTENT_LENGTH; i++) {
		array[i] = m_content[i];
	}

	return array;
}

void
magcard::setContent(std::array<uint8_t, CONTENT_LENGTH> array)
{
	int i;

	for(i = 0; i < CONTENT_LENGTH; i++) {
		m_content[i] = array[i];
	}
}

std::array<double, DATA_REGS>
magcard::getData()
{
	return m_data;
}

void
magcard::setData(std::array<double, DATA_REGS> a)
{
	m_data = a;
}

SIDE_TYPE
magcard::getSide(int side)
{
	switch(side) {
		case 1:
			return m_side1;

		case 2:
			return m_side2;

		default:
			SIDE_TYPE junk;
			junk.fill(0);
			return junk;
	}
}

void
magcard::setSide(int side, SIDE_TYPE s)
{
	switch(side) {
		case 1:
			m_side1 = s;
			m_side1_loaded = true;
			break;

		case 2:
			m_side2 = s;
			m_side2_loaded = true;
			break;

		default:
			break;
	}
}

QString
magcard::getTitle()
{
	return m_title;
}

void
magcard::setTitle(QString s)
{
	//DEBUG_printf("new title >>>%s<<<", qPrintable(s));
	m_title = s;
}

std::array<QString, MAX_LABELS>
magcard::getLabels()
{
	return m_labels;
}

void
magcard::setLabels(std::array<QString, MAX_LABELS> a)
{
	m_labels = a;
}

QString
magcard::getHelp()
{
	return m_help;
}

void
magcard::setHelp(QString s)
{
	m_help = s;
}

STATE_SETTINGS
magcard::getSettings()
{
	return m_settings;
}


// HP67u Calculator Emulator - control and timing
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "calcmain.h"

#include "mainwindow.h"
#include "aar.h"
#include "controlAndTiming.h"
#include "filesys4.h"

controlAndTiming::controlAndTiming()
{
	//DEBUG_printf("control and timing constructed");
}

controlAndTiming::~controlAndTiming()
{
	//DEBUG_printf("control and timing destroyed");
}

void
controlAndTiming::init(CLASSES *cl)
{
	m_cl = cl;

	m_prgm			= false;	// false for RUN, true for PRGM mode
	m_trace			= false;
	m_sst			= false;	// single step through microcode
	m_sleeping		= false;	// stop tracing when waiting for a keypress
	m_power			= true;		// start with power on
	m_stepsPerTick		= 80;		// correct timing for -stk-, -x-, h REG
	m_ticksPerSec		= 40.0;
	m_def_fn		= true;
	m_cat_key		= 0;
	m_keybufIndex		= -1; 		// -1 means empty, else points to top of stack.
	m_cat_waitloopAddress	= 0167;		// HP67 microcode specific.
	m_cat_waitloopEnd	= 0213;		// A loop goes from there to here
	m_cat_count		= 0;
	m_cat_started		= false;	// used here to ensure only one start() thread active.

	memset(m_keybuf, 0, sizeof(m_keybuf));
}

bool
controlAndTiming::getProgram()
{
	return m_prgm;
}

void
controlAndTiming::setProgram(bool n)
{
	m_prgm = n;
}

bool
controlAndTiming::getTrace()
{
	return m_trace;
}

void
controlAndTiming::setTrace(bool n)
{
	m_trace = n;
}

int
controlAndTiming::getStepsPerTick()
{
	return m_stepsPerTick;
}

void
controlAndTiming::setStepsPerTick(int n)
{
	m_stepsPerTick = n;
}

double
controlAndTiming::getTicksPerSec()
{
	return m_ticksPerSec;
}

void
controlAndTiming::setTicksPerSec(double n)
{
	m_ticksPerSec = n;
}

bool
controlAndTiming::getSingleStep()
{
	return m_sst;
}

void
controlAndTiming::setSingleStep(bool n)
{
	m_sst = n;
}

bool
controlAndTiming::getPower()
{
	return m_power;
}

void
controlAndTiming::setPower(bool n)
{
	m_power = n;
}

bool
controlAndTiming::getDisplay()
{
	return m_display;
}

void
controlAndTiming::setDisplay(bool n)
{
	m_display = n;
}

void
controlAndTiming::boot()
{
	m_keybufIndex	= -1; // -1 means "empty"
	m_sleeping	= false;
	m_power		= true;
	m_def_fn	= true;
	m_display	= false;

	if(m_cl->mw->getDebug() == false) {
		// If not debugging, start the world.
		start();
	}
}

void
controlAndTiming::switchPrgm(bool on)
{
	m_prgm = on;
	if(m_sleeping) {
		start();	// need to go and update the display between run ("0.00") and Wprgm "000"
	}
}

// ROM MAP in HP67
//
// 00000-01777 0x0000 quad rom 1 (rom 0x00,0x01,0x02,0x03)
// 02000-03777 0x0400 quad rom 2 (rom 0x04,0x05,0x06,0x07)
// 04000-05777 0x0800 quad rom 3 (rom 0x08,0x09,0x0a,0x0b)
// 06000-07777 0x0c00 quad rom 4 (rom 0x0c,0x0d,0x0e,0x0f)
// 10000-11777 0x1000 empty
// 12000-13777 0x1400 quad rom 5 (bank 1; rom 0x04,0x05,0x06,0x07)

static void *
doStep(void *ptr)
{
	//DEBUG_printf("at top of doStep");
	controlAndTiming *p = (controlAndTiming *)ptr;
	p->step();
	return NULL;
}

void
controlAndTiming::step()
{
	int i;
	int pc, bank, addr, busy;

	while(1) {
		if(!m_power) {
			// we're not going to do anything in here if we're off
			m_cat_started = false;
			//DEBUG_printf("exiting for power off");
			return;
		}

		//DEBUG_printf("at top of thread");
		m_cl->cm->disp();
		if(kbdPressed()) {
			// a key has been pressed
			m_cl->aar->setRegStatus(15, 1);
		}

		for(i = 0; i < m_stepsPerTick; i++) {
			pc = m_cl->aar->getPC();
			bank = m_cl->aar->getBank();

			// multiple instructions per clock cycle to speed up
			//
			// if we're in the wait loop
			if(pc == m_cat_waitloopAddress && bank == 0) {
				// if we've already been here once already, without doing anything outside the loop 
				if(++m_cat_count > 1) {
					int crc_def_fn = m_cl->aar->getCRCdefFn();

					m_sleeping = !m_sleeping;		// flip our state
					if(m_sleeping) {
						m_cl->cm->disp();		// update the display
						if(m_def_fn != crc_def_fn) {	// if card state changed, show it
							m_cl->cm->cmCardDisplay(!crc_def_fn);
						}
						m_def_fn = crc_def_fn;		// and save new state
						m_cat_started = false;
						//DEBUG_printf("exiting for sleeping");
						return;
					}
				}
			}

			// if we're outside the wait loop
			if(bank != 0 || pc < m_cat_waitloopAddress || m_cat_waitloopEnd < pc) {
				m_cat_count=0;	// then we've done 0 loops without events 
			}

			m_cl->aar->setPrevCarry(m_cl->aar->getCarry());
			m_cl->aar->setCarry(0);

			// probably should be any addr w/o a rom in bank 1
			if((pc < 0x400) && (bank == 1))	{
				m_cl->aar->setBank(0); // implicit switch to bank 0
			}

			// pc increments before the instruction is executed, but
			// the instruction address to execute is before the
			// instruction is executed!
			pc = m_cl->aar->getPC();
			bank = m_cl->aar->getBank();
			addr = bank * 4096 + pc++;
			m_cl->aar->setPC(pc);

#if 0
			{
				DEBUG_printf("addr %d", addr);
				DEBUG_printf("A %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegA())));
				DEBUG_printf("B %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegB())));
				DEBUG_printf("C %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegC())));
				DEBUG_printf("D %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegD())));
				DEBUG_printf("E %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegE())));
				DEBUG_printf("F %s", qPrintable(m_cl->aar->regToString(m_cl->aar->getRegF())));
			}
#endif

			m_cl->aar->execute(addr);

			m_cl->aar->setPchange(2, m_cl->aar->getPchange(1));
			m_cl->aar->setPchange(1, m_cl->aar->getPchange(0));
			m_cl->aar->setPchange(0, 0);

			// wrap back to beginning of the ROM
			pc = m_cl->aar->getPC();
			m_cl->aar->setPC(pc & 07777);

			if(m_trace) {
				m_cl->cm->regs();
			}
		}

		busy = m_cl->aar->getBusy();
		if(busy > 0) {
			// prgm memory busy
			m_cl->aar->setBusy(--busy);
		} else {
			m_cl->aar->setRegStatus(5, 1);
		}

		if(m_sst) {
			// needs another start()
			m_cat_started = false;
			//DEBUG_printf("exiting for sst");
			return;
		}

		// Normal running sleeps for 25 ms between instructions.
		usleep(1000000.0 / m_ticksPerSec);
	}
}

void
controlAndTiming::catStartOne()
{
	pthread_t stepThread;
	int rv;

	if(m_cat_started) {
		//DEBUG_printf("already started");
		return;
	}

	m_cat_started = true;
	m_cat_count = 0;		// just started: haven't been through the loop at all yet

	//DEBUG_printf("launch step");
	rv = pthread_create(&stepThread, NULL, doStep, this);
}

void
controlAndTiming::start()
{
	if(m_sst) {
		return;	// don't start if in uc SST mode
	}

	catStartOne();
}

void
controlAndTiming::manualStep()
{
	// pushing the step button also turns on "single step" mode
	m_sst = true;

	catStartOne();
}

void
controlAndTiming::setkey(uint8_t key)
{
	if(++m_keybufIndex >= KEYBUF_DEPTH) {
		// No room...
		--m_keybufIndex;
		return;
	}

	m_keybuf[m_keybufIndex] = key;
	//DEBUG_printf("depth %d", m_keybufIndex);

	start();
}

bool
controlAndTiming::kbdPressed()
{
	if(m_keybufIndex < 0) {
		return false;
	}

	m_cat_key = m_keybuf[m_keybufIndex--];
	//DEBUG_printf("depth %d", m_keybufIndex);

	return true;
}

uint8_t
controlAndTiming::kbdGet()
{
	return m_cat_key;
}


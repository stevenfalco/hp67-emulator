// HP67u Calculator Emulator
//
// Internals->Data
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_5.h"
#include "ui_page_5.h"

#include "calcmain.h"

Page_5::Page_5(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Page_5)
{
	ui->setupUi(this);

	QObject::connect(ui->Clear, SIGNAL(clicked()), this, SLOT(clear()));
	QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
}

Page_5::~Page_5()
{
	delete ui;
}

void 
Page_5::init(CLASSES *cl)
{
	std::array<double, DATA_REGS> data;
	QString s = "";

	int bs = 32;
	char buf[bs];

	m_cl = cl;
		
	data = m_cl->cm->getData();

	for(int i = 0; i < DATA_REGS; i++) {
		snprintf(buf, bs, "%02d: %.9e\n", i, data[i]);
		s += buf;
	}

	ui->text->setPlainText(s);
}

void
Page_5::clear()
{
	std::array<double, DATA_REGS> data;
	QString s = "";

	int bs = 32;
	char buf[bs];

	for(int i = 0; i < DATA_REGS; i++) {
		data[i] = 0.0;
		snprintf(buf, bs, "%02d: %.9e\n", i, data[i]);
		s += buf;
	}
	m_cl->cm->setData(data);

	ui->text->setPlainText(s);
}

void
Page_5::closer()
{
	close();
}

// HP67u Calculator Emulator - main window
//
// Copyright (c) 2023 Steven A. Falco
// 
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "global.h"

#include "aar.h"
#include "calcmain.h"
#include "controlAndTiming.h"
#include "debug.h"
#include "filesys4.h"
#include "magcard.h"
#include "rom67asm.h"
#include "skin.h"

QT_BEGIN_NAMESPACE
namespace Ui {
	class HP67;
}
QT_END_NAMESPACE

// Note that we list rows and columns as 0-based while HP used 1-based when
// storing programs.  Also, they didn't map the slide switches or magcard.
typedef struct {
	int row; // 0 to 9 where 0 is switches, 1-8 are keys, and 9 is magcard.
	int col; // 0 to 4, but many rows don't have that many in use.
} ROW_COL;

class HP67 : public QMainWindow
{
	Q_OBJECT

	public:
		explicit HP67(QWidget *parent = nullptr);

		~HP67();

		void startup(QApplication *app);
		bool getDebug();

	public slots:
		// Power and program switches
		void powerSwitch(int v);
		void runSwitch(int v);

		// Card
		void cardImage(bool v);
		void cardTitle(QString s);
		void cardLabels(std::array<QString, MAX_LABELS> a);

		// Forced resize
		void forcedResize();

	signals:
		void chooseCard();

	private:
		void displayDigit(int position, int v);

		bool eventFilter(QObject *obj, QEvent *event);

		bool keyPress(QKeyEvent *e);
		bool keyRelease(QKeyEvent *e);

		bool mousePress(QGraphicsSceneMouseEvent *e);
		bool mouseRelease(QGraphicsSceneMouseEvent *e);

		void showEvent(QShowEvent *e);
		void resizeEvent(QResizeEvent *e);

		void addFlash(ROW_COL &rc);
		bool delFlash();

		ROW_COL key2rc(int key);

		void programFromFile();
		void programToFile();

		void photoView();
		void enhancedView();

		void getPixmaps();
		void setupPowerView();
		void setupRunView();

		void hideMainWindow();
		void showMainWindow();

		Ui::HP67		*ui;
		QApplication		*m_app;

		bool			m_debug;

		CLASSES			m_classes;
		CLASSES			*m_cl;

		QBrush			m_whiteBrush;

		// Face pixmaps.
		QPixmap			m_faces[FACE_MAX];

		// This is for displaying digits.
		int			m_d_viewWidth;
		int			m_d_viewHeight;
		QPixmap			m_d_pixmaps[DIGIT_MAX];

		// Switches
		QPixmap			m_s_pixmaps[SWITCH_MAX];

		// Card
		QPixmap			m_c_pixmap;

		aar_woodstock		m_aar;
		calcmain		m_cm;
		controlAndTiming	m_cat;
		filesys4		m_fs;
		magcard			m_mag;
		rom67asm		m_rasm;
		skin			m_sk;

		QGraphicsScene		*m_scene;

		QGraphicsPixmapItem	m_facePixmapItem;
		QGraphicsPixmapItem	m_displayItems[DISPLAY_CHARS];
		QGraphicsPixmapItem	m_powerSwitchItem;
		QGraphicsPixmapItem	m_runSwitchItem;
		QGraphicsPixmapItem	m_cardFaceItem;
		QGraphicsTextItem	m_cardTitleItem;
		QGraphicsTextItem	m_cardLabelItems[MAX_LABELS];

		int			m_cardTitleXoffset;
		int			m_cardTitleYoffset;

		int			m_cardLabelXoffset[MAX_LABELS];
		int			m_cardLabelYoffset[MAX_LABELS];

		QGraphicsRectItem	*m_flashRectItem;
		QGraphicsRectItem	*m_hidePixmapItem;

		QFont			m_cardFontBold;
		QFont			m_cardFontNormal;

		double			m_scaleX;
		double			m_scaleY;

		QString			m_display;

	private slots:
		// Display
		void displayString(QString s);

		// Buttons
		void calculatorButtonClicked(ROW_COL rc);

		// Switches and mag card
		void calculatorMagCardClicked();
		void calculatorMagCardText();

		// File menu
		void shutdown();

		// Debugger menu
		void microcode();

		// Internals menu
		void banks();
		void data();
		void program();
		void stack();

		// Help menu
		void about();
		void info();
};

#endif // MAINWINDOW_H

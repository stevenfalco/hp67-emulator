// HP67u Calculator Emulator
//
// Debugger->Microcode
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_9.h"
#include "ui_page_9.h"

#include "calcmain.h"
#include "page_8.h"
#include "skin.h"

Page_9::Page_9(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Page_9)
{
	ui->setupUi(this);

	QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
	QObject::connect(ui->Step, SIGNAL(clicked()), this, SLOT(step()));
	QObject::connect(ui->Faster, SIGNAL(clicked()), this, SLOT(faster()));
	QObject::connect(ui->Slower, SIGNAL(clicked()), this, SLOT(slower()));
	QObject::connect(ui->Turbo, SIGNAL(clicked()), this, SLOT(turbo()));
	QObject::connect(ui->Run, SIGNAL(clicked()), this, SLOT(run()));
	QObject::connect(ui->Log, SIGNAL(clicked()), this, SLOT(log()));
	QObject::connect(ui->Settings, SIGNAL(clicked()), this, SLOT(settings()));
}

Page_9::~Page_9()
{
	delete ui;
}

void 
Page_9::init(CLASSES *cl)
{
	m_cl = cl;

	// We cannot set up these signals in our constructor, because we need
	// the m_cl pointers.
	QObject::connect(m_cl->sk, &skin::sendRegisters, this, &Page_9::showRegisters);
	QObject::connect(m_cl->sk, &skin::refreshPage9, this, &Page_9::refresh);

	// Request the registers.
	m_cl->cm->doShowMCRegs();

	// Show initial modes.
	refresh();
}

void
Page_9::closer()
{
	close();
}

void
Page_9::step()
{
	m_cl->cm->doShowMCRegs();
	m_cl->cm->doStep();
	refresh();
}

void
Page_9::faster()
{
	m_cl->cm->doShowMCRegs();
	m_cl->cm->setSpeedFaster();
	refresh();
}

void
Page_9::slower()
{
	m_cl->cm->doShowMCRegs();
	m_cl->cm->setSpeedSlower();
	refresh();
}

void
Page_9::turbo()
{
	stopLog();
	m_cl->cm->setSpeedTurbo();
	refresh();
}

void
Page_9::run()
{
	stopLog();
	m_cl->cm->setSpeedRun();
	refresh();
}

void
Page_9::stopLog()
{
	int logMode;

	// Turn off the log before going back to full speed.
	while((logMode = m_cl->sk->getLogMode()) != 0) {
		if(++logMode > 3) {
			logMode = 0;
		}
		m_cl->sk->setLogMode(logMode);
	}

}

void
Page_9::log()
{
	int logMode;

	logMode = m_cl->sk->getLogMode();
	if(++logMode > 3) {
		logMode = 0;
	}
	m_cl->sk->setLogMode(logMode);
}

void
Page_9::settings()
{
	Page_8 dialog;

	// Gray out our window.
	setEnabled(false);

	// Give page 8 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	setEnabled(true);
}

void
Page_9::showRegisters(QString s)
{
	ui->Registers->setPlainText(s);
}

void
Page_9::refresh()
{
	ui->Status->setPlainText(m_cl->cm->getSpeed());

	QString s;

	switch(m_cl->sk->getLogMode()) {
		case 0:
			s = "Log is OFF";
			break;

		case 1:
			s = "Log is ON";
			break;

		case 2:
			s = "Log is DETAILED";
			break;

		case 3:
			s = "Log is SMART";
			break;

		default:
			s = "Log is ???";
			break;
	}

	ui->Log->setText(s);
}


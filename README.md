HP67 Calculator Emulator
========================

This project is an adaptation of the javascript project from https://www.sydneysmith.com/wordpress/hp67u/.

I develop on Fedora Linux, and the project should build cleanly for that environment.  It uses Qt version 6 for the gui, so you will need that if you want to build and run the calculator.

It is still a work in progress, but most of the functionality is complete.

To use the emulated magnetic cards, click on the area where mag cards would
show up on the real calculator - i.e. between the slide switches and the
top row of buttons.

Here is a screenshot of the calculator with enhanced visuals:

<img src="images/screenshot.png" >

Here is a screenshot of the calculator with photo-realistic visuals:

<img src="images/screenshot2.png" >

By default, the calculator is started with enhanced visuals, as I find the
"h shifted" keys to be hard to read.  You can use the View menu to select
a photo-realistic view, but I personally prefer the enhanced view, so that
is the default. 

Search for "FACE SELECT" in mainwindow.cpp if you want to change the
default.

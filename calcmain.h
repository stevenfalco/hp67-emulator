// HP67u Calculator Emulator - calcmain
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _CALCMAIN_H_
#define _CALCMAIN_H_

#include "global.h"

class calcmain : public QThread
{
	Q_OBJECT

	public:
		calcmain();
		~calcmain();

		void init(CLASSES *cl);

		bool getCalcKeyDown();
		void setCalcKeyDown(bool n);

		void doPowerOff();
		void doPowerOn();

		void doKey(uint8_t x);

		void doMouseUp();

		void doWpgmOff();
		void doWpgmOn();

		void doCardInsert();

		void disp();

		void calcStartup();
		void doStep();

		void cardRead(uint8_t *dest);
		void cardWrite(uint8_t *src);

		QString getSpeed();
		void setSpeedRun();
		void setSpeedTurbo();
		void setSpeedFaster();
		void setSpeedSlower();

		QString n1(double x);

		bool isTurbo();
		bool isRunning();
		void setRawData(std::array<QString, MAX_REGISTERS> a);
		std::array<QString, MAX_REGISTERS> getRawData();
		std::array<QString, MAX_RAM_ROWS> getAllData();

		double cm_bc2d(std::array<uint8_t, MAX_RAM_COLS> *r);
		std::array<uint8_t, MAX_RAM_COLS> cm_d2bc(double v);

		std::array<double, STACK_ELEMENTS> getStack();

		std::array<double, DATA_REGS> getData();
		void setData(std::array<double, DATA_REGS> data);

		std::array<uint8_t, CONTENT_LENGTH> getProg();
		void setProg(std::array<uint8_t, CONTENT_LENGTH> a);

		void setSettings(STATE_SETTINGS settings);

		QString getD_PC();

		double cm_round(double x);

		QString cm_getdisplay(
				std::array<uint8_t, MAX_RAM_COLS> a,
				std::array<uint8_t, MAX_RAM_COLS> b
				);

		QString printreg(std::array<uint8_t, MAX_RAM_COLS> r);
		void doShowMCRegs();
		void regs();
		void sayDbg(QString msg);
		void cmCardDisplay(bool on);

		QString charForDigit(uint8_t n);

	private:
		std::array<QString, REG_LIST_SIZE> _regs();

		bool m_calcKeyDown;
		bool m_turbo;
		QString m_speed;
		QString m_last_d;

		CLASSES	*m_cl;
};

#endif // _CALCMAIN_H_

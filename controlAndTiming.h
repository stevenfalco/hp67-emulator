// HP67u Calculator Emulator - control and timing
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _CONTROL_AND_TIMING_H_
#define _CONTROL_AND_TIMING_H_

#include <stdint.h>

#include "global.h"

// So far, I've never seen the buffer depth go past 1 item.
#define KEYBUF_DEPTH	16

class controlAndTiming : public QThread
{
	Q_OBJECT

	public:
		controlAndTiming();
		~controlAndTiming();

		void		init(CLASSES *cl);

		bool		getProgram();
		void		setProgram(bool n);

		bool		getTrace();
		void		setTrace(bool n);

		int		getStepsPerTick();
		void		setStepsPerTick(int n);

		double		getTicksPerSec();
		void		setTicksPerSec(double n);

		bool		getSingleStep();
		void		setSingleStep(bool n);

		bool		getPower();
		void		setPower(bool n);

		bool		getDisplay();
		void		setDisplay(bool n);

		void		boot();

		uint8_t		kbdGet();
		void		setkey(uint8_t key);

		void		switchPrgm(bool on);
		void		start();

		void		manualStep();
		void		step();

	private:
		bool		kbdPressed();

		void		catStartOne();

		bool		m_prgm;
		bool		m_trace;
		bool		m_sst;
		bool		m_sleeping;
		bool		m_power;
		bool		m_def_fn;
		bool		m_display;
		bool		m_cat_started;

		int		m_stepsPerTick;
		int		m_keybufIndex;
		int		m_cat_waitloopAddress;
		int		m_cat_waitloopEnd;
		int		m_cat_count;

		CLASSES		*m_cl;

		double		m_ticksPerSec;

		uint8_t		m_cat_key;
		uint8_t		m_keybuf[KEYBUF_DEPTH];
};

#endif // _CONTROL_AND_TIMING_H_

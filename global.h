// HP67u Calculator Emulator - globals
//
// Copyright (c) 2023 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <array>
#include <cerrno>
#include <cmath>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <ctype.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>

#include <QCommandLineParser>
#include <QDebug>
#include <QEvent>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFont>
#include <QFontMetrics>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsTextItem>
#include <QGraphicsView>
#include <QImage>
#include <QKeyEvent>
#include <QMainWindow>
#include <QMessageBox>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QSettings>
#include <QShowEvent>
#include <QTextStream>
#include <QThread>
#include <QVector>
#include <QWidget>

#include "debug.h"

#define HUGE_BUFFER		65536
#define SMALL_BUFFER		512
#define MAX_RAM_COLS		14
#define MAX_REGISTERS		64
#define DATA_REGS		26
#define STATE_LENGTH		7
#define FLAG_LENGTH		4
#define CONTENT_LENGTH		224 // Programs always contain 224 steps
#define STACK_ELEMENTS		5
#define MEM_STATUS_SZ 		16
#define REG_LIST_SIZE		13
#define MAX_RAM_ROWS		160
#define MEM_PARTIAL_STATUS_SZ	12
#define DECIMAL_BASE		10
#define BINARY_BASE		16
#define P_CHANGE_SZ		3
#define RETURN_STACK_SZ		2
#define NIBBLES			7
#define CRC_RAWDATA_SZ		((3 + 1 + (16 * 2) + 2) * NIBBLES) // 38 * 7 = 266

// Magcard related
#define RECORDS_PER_TRACK	38 // Should be 34 as per HP Journal
#define BITS_PER_RECORD		28 // As per HP Journal
#define BITS_PER_CHAR		4  // Because we are using ASCII hex
#define CHARS_PER_RECORD	(BITS_PER_RECORD / BITS_PER_CHAR) // 7 chars
#define CRLF_LENGTH		2
#define NULL_TRAILER_LENGTH	1
#define HEADER_OFFSET_TYPE	21
#define HEADER_SIDES_NEEDED	22 // 1 = just one side, 0 = need two sides?
#define HEADER_OFFSET_FLAGS	23
#define HEADER_OFFSET_ANGLE	24
#define HEADER_OFFSET_PLACES	25
#define HEADER_OFFSET_MODE_H	26
#define HEADER_OFFSET_MODE_L	27

#define MAX_LABELS		10
#define MAX_STRING		256
#define MAX_HELP		4096

// RAM related
#define RAM_SETTINGS_WORD	62
#define RAM_SETTINGS_FLAG_3	0
#define RAM_SETTINGS_FLAG_2	1
#define RAM_SETTINGS_FLAG_1	2
#define RAM_SETTINGS_FLAG_0	3
#define RAM_SETTINGS_MODE_LO	4  // 0 = Scientific or Engineering, 2 = Fixed
#define RAM_SETTINGS_MODE_HI	5  // 2 = Fixed, 0 = Scientific, 4 = Engineering
#define RAM_SETTINGS_PLACES	6  // Number of places displayed

// Display related
#define DISPLAY_CHARS		15 // 15 digit display
#define DIGIT_ZERO		0
#define DIGIT_ONE		1
#define DIGIT_TWO		2
#define DIGIT_THREE		3
#define DIGIT_FOUR		4
#define DIGIT_FIVE		5
#define DIGIT_SIX		6
#define DIGIT_SEVEN		7
#define DIGIT_EIGHT		8
#define DIGIT_NINE		9
#define DIGIT_LC_R		10
#define DIGIT_UC_C		11
#define DIGIT_LC_O		12
#define DIGIT_LC_D		13
#define DIGIT_UC_E		14
#define DIGIT_BLANK		15
#define DIGIT_NEGATIVE		16
#define DIGIT_DECIMAL_POINT	17
#define DIGIT_OFF		18
#define DIGIT_MAX		19

#define SWITCH_LEFT		0
#define SWITCH_RIGHT		1
#define SWITCH_MAX		2

#define POWER_SWITCH_INDEX	0
#define RUN_SWITCH_INDEX	1

#define FACE_ORIGINAL_INDEX	0
#define FACE_ENHANCED_INDEX	1
#define FACE_MAX		2

#define SIDE_TYPE		std::array<uint8_t, CRC_RAWDATA_SZ>

// Forward references.
class HP67;
class aar_woodstock;
class calcmain;
class controlAndTiming;
class filesys4;
class magcard;
class rom67asm;
class skin;

typedef struct {
	HP67			*mw;
	aar_woodstock		*aar;
	calcmain		*cm;
	controlAndTiming	*cat;
	filesys4		*fs;
	magcard			*mag;
	rom67asm		*rasm;
	skin			*sk;
} CLASSES;

// RAM is a 160 x 14 array of integers.  Other things use these defs too.
// However, in other places the amount of RAM is listed as 64 registers in
// four banks.
//
// 00-15 RAM Bank 0 (STO/RCL 0-9,A-E,I)
// 16-31 RAM Bank 1 (prog steps 113-224)
// 32-47 RAM Bank 2 (prog steps 001-112)
// 48-63 RAM Bank 3 (Secondary Regs 0-9 + 6 internal)
// xx-xx Extern   9 (Card Reader internal registers: 0x99 - write data, 0x9b - read data)

typedef struct {
	uint8_t ram[MAX_RAM_ROWS][MAX_RAM_COLS];
} RAM;

// Settings that the magcard parser detects.
typedef struct {
	bool		valid;
	uint8_t		angleMode;
	uint8_t		displayMode;
	uint8_t		displayPlaces;
	uint8_t		flags;
} STATE_SETTINGS;

#endif // _GLOBAL_H_

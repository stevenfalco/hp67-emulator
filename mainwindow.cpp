// HP67u Calculator Emulator - main window
//
// Copyright (c) 2023 Steven A. Falco
// 
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "page_3.h"
#include "page_4.h"
#include "page_5.h"
#include "page_6.h"
#include "page_7.h"
#include "page_9.h"

static QVector<QRect> row1 = {
	QRect(105, 461, 86, 75),	// A
	QRect(237, 461, 86, 75),	// B
	QRect(371, 461, 86, 75),	// C
	QRect(505, 461, 86, 75),	// D
	QRect(638, 461, 86, 75),	// E
};

static QVector<QRect> row2 = {
	QRect(105, 599, 86, 75),	// Σ+
	QRect(237, 599, 86, 75),	// GTO
	QRect(371, 599, 86, 75),	// DSP
	QRect(505, 599, 86, 75),	// (i)
	QRect(638, 599, 86, 75),	// SST
};

static QVector<QRect> row3 = {
	QRect(105, 737, 86, 75),	// f
	QRect(237, 737, 86, 75),	// g
	QRect(371, 737, 86, 75),	// STO
	QRect(505, 737, 86, 75),	// RCL
	QRect(638, 737, 86, 75),	// h
};

static QVector<QRect> row4 = {
	QRect(109, 876, 218, 75),	// ENTER
	QRect(372, 876,  84, 75),	// CHS
	QRect(505, 876,  84, 75),	// EEX
	QRect(637, 876,  84, 75),	// CLX
};

static QVector<QRect> row5 = {
	QRect(108, 1014, 63, 75),	// -
	QRect(244, 1014, 99, 75),	// 7
	QRect(433, 1014, 99, 75),	// 8
	QRect(623, 1014, 99, 75),	// 9
};

static QVector<QRect> row6 = {
	QRect(108, 1152, 63, 75),	// +
	QRect(244, 1152, 99, 75),	// 4
	QRect(433, 1152, 99, 75),	// 5
	QRect(622, 1152, 99, 75),	// 6
};

static QVector<QRect> row7 = {
	QRect(108, 1290, 63, 75),	// ×
	QRect(244, 1290, 99, 75),	// 1
	QRect(433, 1290, 99, 75),	// 2
	QRect(622, 1290, 99, 75),	// 3
};

static QVector<QRect> row8 = {
	QRect(108, 1427, 63, 75),	// ÷
	QRect(244, 1427, 99, 75),	// 0
	QRect(433, 1427, 99, 75),	// .
	QRect(622, 1427, 99, 75),	// R/S
};

static QVector<QRect> switches = {
	QRect(166, 278, 108, 24),	// Power
	QRect(549, 278, 108, 24),	// W/PRGM
};

static QVector<QRect> magcard = {
	QRect(70, 332, 684, 110),	// Magcard
};

static QVector<QVector<QRect>> clickables = {
	row1,
	row2,
	row3,
	row4,
	row5,
	row6,
	row7,
	row8,
	switches,
	magcard,
};

HP67::HP67(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::HP67)
{
	ui->setupUi(this);

	QColor white = Qt::white;
	white.setAlpha(100);
	m_whiteBrush = QBrush(white);

	m_cardFontBold = QFont("Arial", 22, QFont::Bold);
	m_cardFontNormal = QFont("Arial", 18);

	m_flashRectItem = 0;
	m_hidePixmapItem = 0;
	m_scaleX = 1.0;
	m_scaleY = 1.0;

	// Read in the pixmaps.
	getPixmaps();
	
	// Set up the overall scene.
	m_scene = new QGraphicsScene();
	m_scene->installEventFilter(this);
	ui->graphicsView->setScene(m_scene);
	ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	// FACE SELECT: Uncomment only one of the next two lines.
	int defaultFace = FACE_ENHANCED_INDEX;		// Default to enhanced graphics.
	//int defaultFace = FACE_ORIGINAL_INDEX;	// Default to photo-realistic graphics.

	// Install the face.
	m_facePixmapItem.setPixmap(m_faces[defaultFace]);
	m_facePixmapItem.setTransformationMode(Qt::SmoothTransformation);
	m_scene->addItem(&m_facePixmapItem);

	// Install the display digit items.
	for(int i = 0; i < DISPLAY_CHARS; i++) {
		m_displayItems[i].setTransformationMode(Qt::SmoothTransformation);
		m_scene->addItem(&m_displayItems[i]);
	}

	// Install the power switch.
	m_powerSwitchItem.setTransformationMode(Qt::SmoothTransformation);
	m_scene->addItem(&m_powerSwitchItem);

	// Install the W/PRGM / RUN switch.
	m_runSwitchItem.setTransformationMode(Qt::SmoothTransformation);
	m_scene->addItem(&m_runSwitchItem);

	// Install the blank magcard face.
	m_cardFaceItem.setPixmap(m_c_pixmap);
	m_cardFaceItem.setTransformationMode(Qt::SmoothTransformation);
	m_scene->addItem(&m_cardFaceItem);

	// Install the magcard title.
	m_cardTitleItem.setFont(m_cardFontBold);
	m_cardTitleItem.setDefaultTextColor(Qt::white);
	m_scene->addItem(&m_cardTitleItem);

	// Install the magcard labels.
	int half = MAX_LABELS / 2;
	for(int i = 0; i < MAX_LABELS; i++) {
		m_cardLabelItems[i].setFont(m_cardFontNormal);
		if(i < half) {
			m_cardLabelItems[i].setDefaultTextColor(Qt::white);
		} else {
			m_cardLabelItems[i].setDefaultTextColor(QColorConstants::Svg::goldenrod);
		}
		m_scene->addItem(&m_cardLabelItems[i]);
	}

	// Set an initial display string.
	m_display = "               ";
	displayString(m_display);

	// Start with the switches to the right.
	powerSwitch(SWITCH_RIGHT);
	runSwitch(SWITCH_RIGHT);

	// Start out without a memory card.
	cardImage(false);
	cardTitle("");
	std::array<QString, MAX_LABELS> temp;
	for(int i = 0; i < MAX_LABELS; i++) {
		temp[i] = "";
	}
	cardLabels(temp);

	// Mag card
	QObject::connect(ui->actionCard_Access, &QAction::triggered, this, &HP67::calculatorMagCardClicked);
	QObject::connect(ui->actionEdit_Card_Text, &QAction::triggered, this, &HP67::calculatorMagCardText);
	QObject::connect(this, SIGNAL(chooseCard()), &m_sk, SLOT(chooseCard()));

	// Mag card display in gui
	QObject::connect(&m_sk, &skin::cardImage, this, &HP67::cardImage);
	QObject::connect(&m_sk, &skin::cardTitle, this, &HP67::cardTitle);
	QObject::connect(&m_sk, &skin::cardLabels, this, &HP67::cardLabels);
	QObject::connect(&m_sk, &skin::forcedResize, this, &HP67::forcedResize);

	// Display
	QObject::connect(&m_sk, &skin::sendDigit, this, &HP67::displayString);

	// File menu
	QObject::connect(ui->actionProgram_from_file, &QAction::triggered, this, &HP67::programFromFile);
	QObject::connect(ui->actionProgram_to_file, &QAction::triggered, this, &HP67::programToFile);
	QObject::connect(ui->actionExit, &QAction::triggered, this, &HP67::shutdown);

	// View menu
	QObject::connect(ui->actionPhoto_Realistic_View, &QAction::triggered, this, &HP67::photoView);
	QObject::connect(ui->actionEnhanced_Readability_View, &QAction::triggered, this, &HP67::enhancedView);

	// Debugger menu
	QObject::connect(ui->actionMicrocode, &QAction::triggered, this, &HP67::microcode);
	
	// Internals menu
	QObject::connect(ui->actionBanks, &QAction::triggered, this, &HP67::banks);
	QObject::connect(ui->actionData, &QAction::triggered, this, &HP67::data);
	QObject::connect(ui->actionProgram, &QAction::triggered, this, &HP67::program);
	QObject::connect(ui->actionStack, &QAction::triggered, this, &HP67::stack);

	// Help menu
	QObject::connect(ui->actionAbout, &QAction::triggered, this, &HP67::about);
	QObject::connect(ui->actionInfo, &QAction::triggered, this, &HP67::info);

	//DEBUG_printf("starting up");

	// Get references to the classes.
	m_cl = &m_classes;
	m_cl->mw	= this;
	m_cl->aar	= &m_aar;
	m_cl->cm	= &m_cm;
	m_cl->cat	= &m_cat;
	m_cl->fs	= &m_fs;
	m_cl->mag	= &m_mag;
	m_cl->rasm	= &m_rasm;
	m_cl->sk	= &m_sk;
}

HP67::~HP67()
{
	//DEBUG_printf("main window destroyed");
	delete ui;
}

void
HP67::hideMainWindow()
{
	QSize viewSize = ui->graphicsView->size();

	if(!m_hidePixmapItem) {
		m_hidePixmapItem = m_scene->addRect(0, 0, viewSize.width(), viewSize.height(),
				Qt::NoPen, m_whiteBrush);
	}
}

void
HP67::showMainWindow()
{
	if(m_hidePixmapItem) {
		m_scene->removeItem(m_hidePixmapItem);
		delete m_hidePixmapItem;
		m_hidePixmapItem = 0;
	}
}

void
HP67::programFromFile()
{
	QSettings settings;
	const QString DEFAULT_DIR_KEY("default_dir");
	
	hideMainWindow();

	QStringList fnList = {};
	QString fn;

	// Choose / create an appropriate file
	QFileDialog fileDialog;
	fileDialog.setFileMode(QFileDialog::AnyFile);
	fileDialog.setDirectory(settings.value(DEFAULT_DIR_KEY, QDir::homePath()).toString());
	if(fileDialog.exec()) {
		fnList = fileDialog.selectedFiles();
	}

	if(fnList.isEmpty()) {
		showMainWindow();
		return;
	}
	fn = fnList.at(0);

	// Save the path to the selected file for next time.
	QFileInfo fi(fn);
        settings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	QString s = m_cl->fs->load(fn);

	if(!m_cl->mag->fmString(s)) {
		alert("Invalid card");
		showMainWindow();
		return;
	}

	m_cl->cm->setProg(m_cl->mag->getContent());
	m_cl->cm->setData(m_cl->mag->getData());
	m_cl->cm->setSettings(m_cl->mag->getSettings());

	cardImage(true);
	cardTitle(m_cl->mag->getTitle());
	cardLabels(m_cl->mag->getLabels());
	forcedResize();
	showMainWindow();
}

void
HP67::programToFile()
{
	QSettings settings;
	const QString DEFAULT_DIR_KEY("default_dir");
	
	hideMainWindow();

	QStringList fnList = {};
	QString fn;

	// Choose / create an appropriate file
	QFileDialog fileDialog;
	fileDialog.setFileMode(QFileDialog::AnyFile);
	fileDialog.setDirectory(settings.value(DEFAULT_DIR_KEY, QDir::homePath()).toString());
	if(fileDialog.exec()) {
		fnList = fileDialog.selectedFiles();
	}

	if(fnList.isEmpty()) {
		showMainWindow();
		return;
	}
	fn = fnList.at(0);

	// Save the path to the selected file for next time.
	QFileInfo fi(fn);
        settings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	if(fn.isEmpty()) {
		showMainWindow();
		return;
	}

	m_cl->mag->setContent(m_cl->cm->getProg());

	m_cl->fs->save(fn, m_cl->mag->toString());
	showMainWindow();
}

void
HP67::photoView()
{
	m_facePixmapItem.setPixmap(m_faces[FACE_ORIGINAL_INDEX]);
}

void
HP67::enhancedView()
{
	m_facePixmapItem.setPixmap(m_faces[FACE_ENHANCED_INDEX]);
}

void
HP67::shutdown()
{
	exit(0);
}

void
HP67::about()
{
	QMessageBox msg;
	QString s = "";

	QFont font;
        font.setFamilies({QString::fromUtf8("Liberation Mono")});
        font.setPointSize(12);

	s += "HP67 Microcode Emulator Ver 0.03<br>";
	s += "<br>";
	s += "Copyright (C) 2023 Steven A. Falco<br>";
	s += "Copyright (C) 2015-2018 Greg Sydney-Smith<br>";
	s += "<br>";
	s += "This program is free software: you can redistribute it and/or modify it<br>";
	s += "under the terms of the GNU General Public License as published by the<br>";
	s += "Free Software Foundation, either version 3 of the License, or (at your<br>";
	s += "option) any later version.  This program is distributed in the hope that<br>";
	s += "it will be useful, but WITHOUT ANY WARRANTY; without even the implied<br>";
	s += "warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>";
	s += "<br>";
	s += "See the GNU General Public License for more details.  You should have<br>";
	s += "received a copy of the GNU General Public License along with this<br>";
	s += "program. If not, see <a href='https://www.gnu.org/licenses'>www.gnu.org</a>.";

	msg.setStyleSheet("QLabel{min-width: 700px;}");
	msg.setText(s);
	msg.setFont(font);
	msg.setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	msg.exec();
}

void
HP67::info()
{
	QMessageBox msg;
	QString s = "";

	QFont font;
        font.setFamilies({QString::fromUtf8("Liberation Mono")});
        font.setPointSize(12);

	s += "For more information about this project, please visit the gitlab ";
	s += "page here: <a href='https://gitlab.com/stevenfalco/hp67-emulator'>HP67</a>.<br><br>";
	s += "For more information about the javascript implementation that this program derives from, ";
	s += "along with a lot of information about HP calculator internals, please visit ";
        s += "<a href='https://www.sydneysmith.com/'>www.sidneysmith.com</a>.<br><br>";
	s += "Keyboard shortcuts:<br><br>";

	s += "u = \u03a3+<br>";
	s += "t = GTO<br>";
	s += "m = DSP (display mode)<br>";
	s += "i = (i)<br>";
	s += "l = SST (and label)<br>";
	s += "spacebar = R/S<br><br>";

	s += "f = f shift (gold shift key)<br>";
	s += "g = g shift (blue shift key)<br>";
	s += "h = h shift (black shift key)<br>";
	s += "s = STO<br>";
	s += "r = RCL<br><br>";

	s += "return (or enter) = ENTER<br>";
	s += "~ = CHS<br>";
	s += "x = EEX<br>";
	s += "backspace = CLX<br><br>";

	s += "* = \u00d7<br>";
	s += "/ = \u00f7<br><br>";

	s += "p = POWER slide switch<br>";
	s += "w = W/PRGM slide switch<br>";
	s += "k = MagCard<br><br>";

	s += "a-e are the 'soft' keys, and of course 0-9 and '.' do what you'd expect.";

	msg.setText(s);
	msg.setFont(font);
	msg.setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	msg.exec();
}

void
HP67::calculatorMagCardText()
{
	Page_4 dialog;

	// Gray out our window.
	hideMainWindow();

	// Give page 4 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::banks()
{
	Page_7 dialog;

	// Gray out our window.
	hideMainWindow();

	// Give page 7 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::data()
{
	Page_5 dialog;

	// Gray out our window.
	hideMainWindow();

	// Give page 5 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::program()
{
	Page_6 dialog;

	// Gray out our window.
	hideMainWindow();

	// Give page 6 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::microcode()
{
	Page_9 dialog;

	// Gray out our window.
	hideMainWindow();

	// Give page 9 access to the various classes.
	dialog.init(m_cl);

	// Run the dialog.
	dialog.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::stack()
{
	QMessageBox msg;
	QString s = "";

	QFont font;
        font.setFamilies({QString::fromUtf8("Liberation Mono")});
        font.setPointSize(12);

	std::array<double, STACK_ELEMENTS> stack = m_cl->cm->getStack();

	int bs = 32;
	char buf[bs];

	// Gray out our window.
	hideMainWindow();

	s += "Calculator Stack Contents\n\n";

	snprintf(buf, bs, "T: %.9e\n", stack[4]);
	s += buf;

	snprintf(buf, bs, "Z: %.9e\n", stack[3]);
	s += buf;

	snprintf(buf, bs, "Y: %.9e\n", stack[2]);
	s += buf;

	snprintf(buf, bs, "X: %.9e\n", stack[1]);
	s += buf;

	snprintf(buf, bs, "\nLastX: %.9e\n", stack[0]);
	s += buf;

	msg.setText(s);
	msg.setFont(font);
	msg.setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
	msg.exec();

	// Restore our window.
	showMainWindow();
}

void
HP67::getPixmaps()
{
	// Face pixmaps - these two have the same dimensions.
	m_faces[FACE_ORIGINAL_INDEX]		= QPixmap(QString::fromUtf8(":images/face1.png"));
	m_faces[FACE_ENHANCED_INDEX]		= QPixmap(QString::fromUtf8(":images/face2.png"));

	// Digit pixmaps
	m_d_pixmaps[DIGIT_ZERO]			= QPixmap(QString::fromUtf8(":/images/dig0.png"));
	m_d_pixmaps[DIGIT_ONE]			= QPixmap(QString::fromUtf8(":/images/dig1.png"));
	m_d_pixmaps[DIGIT_TWO]			= QPixmap(QString::fromUtf8(":/images/dig2.png"));
	m_d_pixmaps[DIGIT_THREE]		= QPixmap(QString::fromUtf8(":/images/dig3.png"));
	m_d_pixmaps[DIGIT_FOUR]			= QPixmap(QString::fromUtf8(":/images/dig4.png"));
	m_d_pixmaps[DIGIT_FIVE]			= QPixmap(QString::fromUtf8(":/images/dig5.png"));
	m_d_pixmaps[DIGIT_SIX]			= QPixmap(QString::fromUtf8(":/images/dig6.png"));
	m_d_pixmaps[DIGIT_SEVEN]		= QPixmap(QString::fromUtf8(":/images/dig7.png"));
	m_d_pixmaps[DIGIT_EIGHT]		= QPixmap(QString::fromUtf8(":/images/dig8.png"));
	m_d_pixmaps[DIGIT_NINE]			= QPixmap(QString::fromUtf8(":/images/dig9.png"));
	m_d_pixmaps[DIGIT_LC_R]			= QPixmap(QString::fromUtf8(":/images/diga.png"));
	m_d_pixmaps[DIGIT_UC_C]			= QPixmap(QString::fromUtf8(":/images/digb.png"));
	m_d_pixmaps[DIGIT_LC_O]			= QPixmap(QString::fromUtf8(":/images/digc.png"));
	m_d_pixmaps[DIGIT_LC_D]			= QPixmap(QString::fromUtf8(":/images/digd.png"));
	m_d_pixmaps[DIGIT_UC_E]			= QPixmap(QString::fromUtf8(":/images/dige.png"));
	m_d_pixmaps[DIGIT_BLANK]		= QPixmap(QString::fromUtf8(":/images/digf.png"));
	m_d_pixmaps[DIGIT_NEGATIVE]		= QPixmap(QString::fromUtf8(":/images/digneg.png"));
	m_d_pixmaps[DIGIT_DECIMAL_POINT]	= QPixmap(QString::fromUtf8(":/images/digdot.png"));
	m_d_pixmaps[DIGIT_OFF]			= QPixmap(QString::fromUtf8(":/images/digoff.png"));

	// Switch pixmaps
	m_s_pixmaps[SWITCH_LEFT]		= QPixmap(QString::fromUtf8(":/images/switch_left.png"));
	m_s_pixmaps[SWITCH_RIGHT]		= QPixmap(QString::fromUtf8(":/images/switch_right.png"));
	
	// Card pixmap
	m_c_pixmap				= QPixmap(QString::fromUtf8(":/images/card.png"));
}

void
HP67::startup(QApplication *app)
{
	m_app = app;

	QCommandLineParser parser;

	parser.setApplicationDescription("HP67");
	parser.addHelpOption();
	parser.addVersionOption();

	QCommandLineOption debugOption("d", QCoreApplication::translate("main", "run in debug mode"));
	parser.addOption(debugOption);

	parser.process(*m_app);

	m_debug = parser.isSet(debugOption);

	// Do initializations.
	m_cl->cm->init(m_cl); // calcmain handles aar and cat in doPowerOn().
	m_cl->sk->init(m_cl);
	m_cl->mag->init(m_cl);

	m_cl->sk->startup();
}

bool
HP67::getDebug()
{
	return m_debug;
}

bool
HP67::eventFilter(QObject *obj, QEvent *event)
{
	//qInfo() << "event " << event;
	//qInfo() << "event type " << event->type();

	if(event->type() == QEvent::KeyPress) {
		return keyPress(static_cast<QKeyEvent *>(event));
	} else if(event->type() == QEvent::KeyRelease) {
		return keyRelease(static_cast<QKeyEvent *>(event));
	} else if(event->type() == QEvent::GraphicsSceneMousePress) {
		return mousePress(static_cast<QGraphicsSceneMouseEvent *>(event));
	} else if(event->type() == QEvent::GraphicsSceneMouseRelease) {
		return mouseRelease(static_cast<QGraphicsSceneMouseEvent *>(event));
	} else {
		// standard event processing
		return QObject::eventFilter(obj, event);
	}
}

void
HP67::addFlash(ROW_COL &rc)
{
	QRect rect = clickables[rc.row][rc.col];

	QRectF flashAt(rect.left() * m_scaleX, rect.top() * m_scaleY,
			rect.width() * m_scaleX, rect.height() * m_scaleY);

	m_flashRectItem = m_scene->addRect(flashAt, Qt::NoPen, m_whiteBrush);
	//qInfo() << "added at" << flashAt;
}

bool
HP67::delFlash()
{
	bool rv = false;

	if(m_flashRectItem) {
		m_scene->removeItem(m_flashRectItem);
		delete m_flashRectItem;
		m_flashRectItem = 0;
		rv = true;
		//qInfo() << "removed";
	}

	return rv;
}

ROW_COL
HP67::key2rc(int key)
{
	ROW_COL rc;

	switch(key) {
		// HP row 1 - soft keys
		case Qt::Key_A:			rc.row = 0; rc.col = 0; return rc;
		case Qt::Key_B:			rc.row = 0; rc.col = 1; return rc;
		case Qt::Key_C:			rc.row = 0; rc.col = 2; return rc;
		case Qt::Key_D:			rc.row = 0; rc.col = 3; return rc;
		case Qt::Key_E:			rc.row = 0; rc.col = 4; return rc;

		// HP row 2 - control keys
		case Qt::Key_U:			rc.row = 1; rc.col = 0; return rc; // Sigma
		case Qt::Key_T:			rc.row = 1; rc.col = 1; return rc; // GoTo
		case Qt::Key_M:			rc.row = 1; rc.col = 2; return rc; // Display mode
		case Qt::Key_I:			rc.row = 1; rc.col = 3; return rc; // (i)
		case Qt::Key_L:			rc.row = 1; rc.col = 4; return rc; // SST and Label

		// HP row 3 - shift keys and store/recall
		case Qt::Key_F:			rc.row = 2; rc.col = 0; return rc; // f
		case Qt::Key_G:			rc.row = 2; rc.col = 1; return rc; // g
		case Qt::Key_S:			rc.row = 2; rc.col = 2; return rc; // STO
		case Qt::Key_R:			rc.row = 2; rc.col = 3; return rc; // RCL
		case Qt::Key_H:			rc.row = 2; rc.col = 4; return rc; // h

		// HP row 4 - Enter, CHS, EEX, CLX
		case Qt::Key_Return:		rc.row = 3; rc.col = 0; return rc; // Enter
		case Qt::Key_Enter:		rc.row = 3; rc.col = 0; return rc; // Enter
		case Qt::Key_AsciiTilde:	rc.row = 3; rc.col = 1; return rc; // CHS
		case Qt::Key_X:			rc.row = 3; rc.col = 2; return rc; // EEX
		case Qt::Key_Backspace:		rc.row = 3; rc.col = 3; return rc; // CLX

		// HP row 5 - Minus and some digits
		case Qt::Key_Minus:		rc.row = 4; rc.col = 0; return rc;
		case Qt::Key_7:			rc.row = 4; rc.col = 1; return rc;
		case Qt::Key_8:			rc.row = 4; rc.col = 2; return rc;
		case Qt::Key_9:			rc.row = 4; rc.col = 3; return rc;

		// HP row 6 - Add and some digits
		case Qt::Key_Plus:		rc.row = 5; rc.col = 0; return rc;
		case Qt::Key_4:			rc.row = 5; rc.col = 1; return rc;
		case Qt::Key_5:			rc.row = 5; rc.col = 2; return rc;
		case Qt::Key_6:			rc.row = 5; rc.col = 3; return rc;

		// HP row 7 - Multiply and some digits
		case Qt::Key_Asterisk:		rc.row = 6; rc.col = 0; return rc;
		case Qt::Key_1:			rc.row = 6; rc.col = 1; return rc;
		case Qt::Key_2:			rc.row = 6; rc.col = 2; return rc;
		case Qt::Key_3:			rc.row = 6; rc.col = 3; return rc;

		// HP row 8 - Divid, zero, decimal point, run/stop
		case Qt::Key_Slash:		rc.row = 7; rc.col = 0; return rc;
		case Qt::Key_0:			rc.row = 7; rc.col = 1; return rc;
		case Qt::Key_Period:		rc.row = 7; rc.col = 2; return rc;
		case Qt::Key_Space:		rc.row = 7; rc.col = 3; return rc; // run/stop

		// Switches
		case Qt::Key_P:			rc.row = 8; rc.col = 0; return rc; // power on/off
		case Qt::Key_W:			rc.row = 8; rc.col = 1; return rc; // W/PRGM

		// Mag card
		case Qt::Key_K:			rc.row = 9; rc.col = 0; return rc;

		default:			rc.row = -1; rc.col = -1; return rc;
	}
}

void
HP67::calculatorMagCardClicked()
{
	emit chooseCard();
}

void
HP67::calculatorButtonClicked(ROW_COL rc)
{
	uint8_t kc;

	if(rc.row != 9) {
		addFlash(rc);
	}

	switch(rc.row) {
		case 0:
			switch(rc.col) {
				case 0: kc = 0xa4; break;
				case 1: kc = 0xa3; break;
				case 2: kc = 0xa2; break;
				case 3: kc = 0xa1; break;
				case 4: kc = 0xa0; break;
				default: return;
			}
			break;

		case 1:
			switch(rc.col) {
				case 0: kc = 0x94; break;
				case 1: kc = 0x93; break;
				case 2: kc = 0x92; break;
				case 3: kc = 0x91; break;
				case 4: kc = 0x90; break;
				default: return;
			}
			break;

		case 2:
			switch(rc.col) {
				case 0: kc = 0x14; break;
				case 1: kc = 0x13; break;
				case 2: kc = 0x12; break;
				case 3: kc = 0x11; break;
				case 4: kc = 0x10; break;
				default: return;
			}
			break;

		case 3:
			switch(rc.col) {
				case 0: kc = 0x33; break;
				case 1: kc = 0x32; break;
				case 2: kc = 0x31; break;
				case 3: kc = 0x30; break;
				default: return;
			}
			break;

		case 4:
			switch(rc.col) {
				case 0: kc = 0x43; break;
				case 1: kc = 0x42; break;
				case 2: kc = 0x41; break;
				case 3: kc = 0x40; break;
				default: return;
			}
			break;

		case 5:
			switch(rc.col) {
				case 0: kc = 0x53; break;
				case 1: kc = 0x52; break;
				case 2: kc = 0x51; break;
				case 3: kc = 0x50; break;
				default: return;
			}
			break;

		case 6:
			switch(rc.col) {
				case 0: kc = 0x63; break;
				case 1: kc = 0x62; break;
				case 2: kc = 0x61; break;
				case 3: kc = 0x60; break;
				default: return;
			}
			break;

		case 7:
			switch(rc.col) {
				case 0: kc = 0x73; break;
				case 1: kc = 0x72; break;
				case 2: kc = 0x71; break;
				case 3: kc = 0x70; break;
				default: return;
			}
			break;

		case 8:
			switch(rc.col) {
				case 0: m_cl->sk->OnPage1Power(); return;
				case 1: m_cl->sk->OnPage1Wpgm(); return;
				default: return;
			}
			break;

		case 9:
			switch(rc.col) {
				case 0: calculatorMagCardClicked(); return;
				default: return;
			}
			break;

		default:
			return;
	}

	m_cl->cm->doKey(kc);
}

bool
HP67::keyPress(QKeyEvent *e)
{
	//qInfo() << "pressed " << e->key();

	ROW_COL rc = key2rc(e->key());

	if(rc.row < 0 || rc.col < 0) {
		return false;
	}

	calculatorButtonClicked(rc);

	return true;
}

bool
HP67::keyRelease(QKeyEvent *e)
{
	//qInfo() << "released " << e->key();

	m_cl->cm->doMouseUp();
	return delFlash();
}

bool
HP67::mousePress(QGraphicsSceneMouseEvent *e)
{
	ROW_COL rc;

	// Always in units of the unscaled face pixmap.
	QRect rect;

	// Scale the position up to the face pixmap size so we can search
	// for where the click landed.
	QPointF position = e->scenePos();
	position.rx() /= m_scaleX;
	position.ry() /= m_scaleY;

	// Search to see if the click was at a y-position that we care about.
	for(rc.row = 0; rc.row < clickables.size(); rc.row++) {
		// All members of a row are at the same height so we can just
		// use element 0 when searching.
		rect = clickables[rc.row][0];
		if(position.y() >= rect.top() && position.y() < rect.bottom()) {
			break;
		}
	}

	if(rc.row >= clickables.size()) {
		// The y-position is outside of any row that we care about so
		// pass the click off in case someone else wants it.
		return false;
	}

	// Search to see if the click was at an x-position that we care about.
	for(rc.col = 0; rc.col < clickables[rc.row].size(); rc.col++) {
		rect = clickables[rc.row][rc.col];
		if(position.x() >= rect.left() && position.x() < rect.right()) {
			break;
		}
	}
	if(rc.col >= clickables[rc.row].size()) {
		// The y-position is outside of any column that we care about
		// so pass the click off in case someone else wants it.
		return false;
	}

	//qInfo() << "row and column" << rc.row << rc.col;

	calculatorButtonClicked(rc);

	return true;
}

bool
HP67::mouseRelease(QGraphicsSceneMouseEvent *e)
{
	//qInfo() << "released " << e->button();

	m_cl->cm->doMouseUp();
	return delFlash();
}

void
HP67::forcedResize()
{
	resizeEvent(0);
}

void
HP67::showEvent(QShowEvent *e)
{
	resizeEvent(0);
}

void
HP67::resizeEvent(QResizeEvent *e)
{
	QRect posn;

	// Get the sizes of the graphics view and the face.
	QSize viewSize = ui->graphicsView->size();
	QSize faceSize = m_faces[FACE_ENHANCED_INDEX].size();

	// Figure out the scale factors.
	m_scaleX = double(viewSize.width()) / double(faceSize.width());
	m_scaleY = double(viewSize.height()) / double(faceSize.height());
	//DEBUG_printf("m_scaleX %f  m_scaleY %f", m_scaleX, m_scaleY);

	// Create a transformation based on the scale factors.
	QTransform transform = QTransform::fromScale(m_scaleX, m_scaleY);

	// Scale the face.
	m_facePixmapItem.setTransform(transform);
	m_cardFaceItem.setPos(0, 0);

	// Scale the digits.
	for(int i = 0; i < DISPLAY_CHARS; i++) {
		m_displayItems[i].setTransform(transform);
		m_displayItems[i].setPos(m_scaleX * ((i * 40) + 110), m_scaleY * 110);
	}

	// Scale the switches.
	posn = switches[POWER_SWITCH_INDEX];
	m_powerSwitchItem.setTransform(transform);
	m_powerSwitchItem.setPos(m_scaleX * posn.left(), m_scaleY * posn.top());

	posn = switches[RUN_SWITCH_INDEX];
	m_runSwitchItem.setTransform(transform);
	m_runSwitchItem.setPos(m_scaleX * posn.left(), m_scaleY * posn.top());

	// Scale the magcard items.
	posn = magcard[0];
	m_cardFaceItem.setTransform(transform);
	m_cardFaceItem.setPos(m_scaleX * posn.left(), m_scaleY * posn.top());

	m_cardTitleItem.setTransform(transform);
	m_cardTitleItem.setPos(m_scaleX * m_cardTitleXoffset, m_scaleY * m_cardTitleYoffset);

	for(int i = 0; i < MAX_LABELS; i++) {
		m_cardLabelItems[i].setTransform(transform);
		m_cardLabelItems[i].setPos(m_scaleX * m_cardLabelXoffset[i], m_scaleY * m_cardLabelYoffset[i]);
	}
}

// We are given a string like " 0.00fffffff    ".  There are individual
// graphics for each character, with the ones at 10 and above used for
// spelling out Error and Crd messages.
void
HP67::displayString(QString s)
{
	int i;
	char c;

	// 15 character positions.
	for(i = 0; i < DISPLAY_CHARS; i++) {
		if(i < s.size()) {
			c = s[i].toLatin1();
		} else {
			c = ' ';
		}

		if(c == '.') {
			// Decimal point.
			displayDigit(i, DIGIT_DECIMAL_POINT);
		} else if(c == '-') {
			// Minus sign for mantissa and/or exponent.
			displayDigit(i, DIGIT_NEGATIVE);
		} else if(c == ' ') {
			// Blank position.
			displayDigit(i, DIGIT_OFF);
		} else if(c >= '0' && c <= '9') {
			// Normal digit.
			displayDigit(i, c - '0');
		} else if(c >= 'a' && c <= 'f') {
			// Special character.
			displayDigit(i, (c - 'a') + 10);
		}
	}
}

void
HP67::displayDigit(int position, int v)
{
	if(v < 0 || v >= DIGIT_MAX) {
		//qInfo() << "v too big" << v;
		return;
	}

	if(position < 0 || position >= DISPLAY_CHARS) {
		//qInfo() << "position too big" << position;
		return;
	}

	m_displayItems[position].setPixmap(m_d_pixmaps[v]);
}

void
HP67::powerSwitch(int v)
{
	if(v < 0 || v >= SWITCH_MAX) {
		return;
	}

	m_powerSwitchItem.setPixmap(m_s_pixmaps[v]);
}

void
HP67::runSwitch(int v)
{
	if(v < 0 || v >= SWITCH_MAX) {
		return;
	}

	m_runSwitchItem.setPixmap(m_s_pixmaps[v]);
}

void
HP67::cardImage(bool v)
{
	// Change the visibility as requested.
	m_cardFaceItem.setVisible(v);
	m_cardTitleItem.setVisible(v);
	for(int i = 0; i < MAX_LABELS; i++) {
		m_cardLabelItems[i].setVisible(v);
	}
}

void
HP67::cardTitle(QString s)
{
	QFontMetrics cardMetricsBold(m_cardFontBold);
	QRect mag = magcard[0];

	// We want to center the title, but it might be too wide to fit in the
	// scene.  If so, we put it up against the left margin, and let the
	// scene clip it for us.
	int textWidth = cardMetricsBold.horizontalAdvance(s);
	m_cardTitleXoffset = mag.left() + ((mag.width() >= textWidth) ? (mag.width() - textWidth) / 2 : 0);
	m_cardTitleYoffset = mag.top();

	//qInfo() << "title offset" << m_cardTitleXoffset << m_cardTitleYoffset;

	m_cardTitleItem.setPlainText(s);
}

void
HP67::cardLabels(std::array<QString, MAX_LABELS> a)
{
	QFontMetrics cardMetricsNormal(m_cardFontNormal);
	QRect mag = magcard[0];

	int fieldStart;
	int textWidth;
	int half = MAX_LABELS / 2;
	int shift = 6;
	int each = (mag.width() - (2 * shift)) / 5;

	for(int i = 0; i < MAX_LABELS; i++) {
		textWidth = cardMetricsNormal.horizontalAdvance(a[i]);
		if(i < half) {
			fieldStart = (i * each) + shift;
			m_cardLabelYoffset[i] = mag.top() + mag.height() * 0.60;
		} else {
			fieldStart = ((i - half) * each) + shift;
			m_cardLabelYoffset[i] = mag.top() + mag.height() * 0.35;
		}
		m_cardLabelXoffset[i] = mag.left() + fieldStart + ((each >= textWidth) ? ((each - textWidth) / 2) : 0);

		//qInfo() << "label" << i << "offset" << m_cardTitleXoffset << m_cardTitleYoffset;

		m_cardLabelItems[i].setPlainText(a[i]);
	}
}


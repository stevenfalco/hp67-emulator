// HP67u Calculator Emulator
//
// Internals->Data
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef PAGE_5_H
#define PAGE_5_H

#include <QDialog>

#include "global.h"

namespace Ui {
	class Page_5;
}

class Page_5 : public QDialog
{
	Q_OBJECT

	public:
		explicit Page_5(QWidget *parent = nullptr);
		~Page_5();

		void init(CLASSES *cl);

	private slots:
		void clear();
		void closer();

	private:
		Ui::Page_5 *ui;

		CLASSES *m_cl;
};

#endif // PAGE_5_H

// HP67u Calculator Emulator
//
// Debugger->Microcode->Memory and Program Counter
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_8.h"
#include "ui_page_8.h"

#include "aar.h"
#include "calcmain.h"

Page_8::Page_8(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Page_8)
{
    ui->setupUi(this);

    QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
}

Page_8::~Page_8()
{
    delete ui;
}

void 
Page_8::init(CLASSES *cl)
{
	int i;

	char buf[SMALL_BUFFER];

	m_cl = cl;

	std::array<QString, MAX_REGISTERS> a = m_cl->cm->getRawData();

	// '67 has 64 registers, 4 banks of 16 each.
	m_text.clear();
	for(i = 0; i < MAX_REGISTERS; i++) {
		snprintf(buf, SMALL_BUFFER, "d%02d: %s\n", i, qPrintable(a[i]));
		m_text += buf;
	}

	m_text += m_cl->cm->getD_PC();

	ui->text->setPlainText(m_text);
}

void
Page_8::closer()
{
	std::array<QString, MAX_REGISTERS> a;

	unsigned long addr;

	size_t pos = 0;

	int pc = 0;
	int bank = 0;

	QString line;
	QString colon_space = ": ";
	QString nl = "\n";
	QString lhs;

	QString s = ui->text->toPlainText();

	if(s != m_text) {
		while(1) {
			// Find the end of a line.
			if((pos = s.indexOf(nl)) == -1) {
				break;
			}

			// Extract the entry and delete it from the
			// source string.
			line = s.mid(0, pos);
			s.remove(0, pos + nl.length());

			// Find the next delimiter
			if((pos = line.indexOf(colon_space)) == -1) {
				// Line is malformed - ignore it.
				continue;
			}

			// Get the left hand side, and remove it from
			// the line.  What will remain is the right hand
			// side.
			lhs = line.mid(0, pos);
			line.remove(0, pos + colon_space.length());

			if((lhs.length() == 3)
					&& (lhs.at(0) == 'd')
				       	&& (lhs.at(1).isDigit())
				       	&& (lhs.at(2).isDigit())
					&& (line.length() == MAX_RAM_COLS)
					)
			{
				// Memory location.  Get the address and
				// save the value if it is in range.
				addr = strtoul(qPrintable(lhs) + 1, 0, 10);
				if(addr < MAX_REGISTERS) {
					a[addr] = line;
				}
			} else if(lhs == "pc:") {
				addr = strtoul(qPrintable(line), 0, 8);
				pc = addr & 07777;
				bank = addr >> 10;

				m_cl->aar->setPC(pc);
				m_cl->aar->setBank(bank);
			}
		}

		m_cl->cm->setRawData(a);
	}

	close();
}

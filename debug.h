// HP67u Calculator Emulator
//
// Copyright (c) 2023 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef __DEBUG_H_
#define __DEBUG_H_

#define ENABLE_DEBUG

void _DEBUG_printf(const char *fn, int ln, const char *format, ...)
	__attribute__ ((format (printf, 3, 4)));

#ifdef ENABLE_DEBUG
#define DEBUG_printf(format, ...) \
	_DEBUG_printf(__PRETTY_FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#else
#define DEBUG_printf(format, ...)
#endif

void alert(const char *format, ...)
	__attribute__ ((format (printf, 1, 2)));

#endif // __DEBUG_H_

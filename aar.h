// HP67u Calculator Emulator - AAR module
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _AAR_WOODSTOCK_H_
#define _AAR_WOODSTOCK_H_

#include "global.h"

class aar_woodstock : public QThread
{
	Q_OBJECT

	public:
		aar_woodstock();
		~aar_woodstock();

		void init(CLASSES *cl);

		int getCarry();
		void setCarry(int n);

		int getPrevCarry();
		void setPrevCarry(int n);

		int getPchange(int n);
		void setPchange(int n, int v);

		int getBank();
		void setBank(int n);

		int getPC();
		void setPC(int n);

		int getBusy();
		void setBusy(int n);

		uint8_t getRegStatus(int element);
		void setRegStatus(int element, uint8_t value);

		RAM *getRAM();
		void setRAM(RAM *r);

		void disptoggle();
		void dispoff();
		void op_if_nc_goto(int addr);
		void op_then_goto(int addr);
		void op_jsb(int addr);
		void op_return();
		void op_set_s(int num, int val);
		void op_if_s_eq_0(int N);
		void op_if_s_eq_1(int N);
		void setp(int val);
		void op_if_p_ne(int N);
		void op_if_p_eq(int N);
		void op_inc_p();
		void decp();
		void op_dec_p();
		void negc(int first, int last);
		void negsubc(int first, int last);
		void nop();
		void op_stack_to_a();
		void downrot();
		void op_clear_s();
		void op_c_to_stack();
		void loadconst(int num);
		void op_del_sel_rom(int N);
		void op_c_to_addr();
		void op_clear_data_regs();
		void op_c_to_data();
		void op_data_to_c();
		void op_c_to_data_register(int N);
		void op_data_register_to_c(int n);
		void op_circulate_a_left();
		void op_binary();
		void crc_start();
		void op_crc(int N);
		void op_keys_to_a();
		void op_a_to_rom_addr();
		void op_display_reset_twf();
		void op_decimal();
		void op_bank_switch();
		void CardWrite();
		void CardRead();
		bool getDisplay14Digit();
		std::array<uint8_t, MAX_RAM_COLS> getRegA();
		std::array<uint8_t, MAX_RAM_COLS> getRegB();
		std::array<uint8_t, MAX_RAM_COLS> getRegC();
		std::array<uint8_t, MAX_RAM_COLS> getRegD();
		std::array<uint8_t, MAX_RAM_COLS> getRegE();
		std::array<uint8_t, MAX_RAM_COLS> getRegF();
		std::array<uint8_t, MAX_RAM_COLS> getRegM1();
		std::array<uint8_t, MAX_RAM_COLS> getRegM2();
		int getP();
		int getCRCdefFn();

		void setreg(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b,
				int first, int last);
		void op_set_reg_zero(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void increg(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void decreg(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void exchreg(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b,
				int first, int last);
		void shiftl(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void shiftr(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void op_if_r_eq_0(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void sub(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b, 
				std::array<uint8_t, MAX_RAM_COLS> *c,
				int first, int last);
		void add(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b, 
				std::array<uint8_t, MAX_RAM_COLS> *c,
				int first, int last);
		void op_if_r_ge_r(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b,
				int first, int last);
		void op_if_r_ne_0(
				std::array<uint8_t, MAX_RAM_COLS> *r,
				int first, int last);
		void op_m_exch_c(
				std::array<uint8_t, MAX_RAM_COLS> *r);
		void op_r_to_r(
				std::array<uint8_t, MAX_RAM_COLS> *a, 
				std::array<uint8_t, MAX_RAM_COLS> *b);

		QString regToString(std::array<uint8_t, MAX_RAM_COLS> r);

		QString statusToString();

		// This is in a separate file because it is huge.
		void execute(int routineNumber);

	private:
		int doAdd(int x, int y);
		int doSub(int x, int y);
		int field(int val);
		void negcdec(int first, int last, int dec);
		void op_crc_1700();
		char *fmtRam(uint8_t *r, int n);

		CLASSES *m_cl;

		// What follows is from:
		//
		// https://www.sydneysmith.com/wordpress/1910/the-new-calculator-debuggers/
		//
		// Most registers in the real hardware are 56 bits wide,
		// but the S register is 16 bits wide.
		//
		// The main registers inside the microcode CPU are
		// the A-F registers, the M1 and M2 registers, the
		// S register and the P register. 
		//
		// A-F, M1 and M2 all hold 14 binary coded decimal values.
		// As BCD values take 4 bits, we have 4 * 14 = 56 bits
		// for the registers.  This code uses 14 chars for the
		// registers, which wastes some bits.
		//
		// For most of the calculator’s time, it will have ordinary
		// numbers stored in the registers. These are values that
		// it will add, subtract, multiply, divide, square root, etc.
		//
		// Ordinary numbers appear in the registers as:
		//
		// smmmmmmmmmmexx
		//
		// where "s" is the sign of the number ("0" for positive,
		// "9" for negative), "m" is the mantissa (explained shortly),
		// "e" is the exponent sign and "x" is the exponent.
		//
		// Internally, the calculator uses scientific notation for
		// every number operation. So, "23" decimal appears in the
		// registers as 02300000000001
		//
		// Like the prior calculator models, the A and B registers are
		// connected to the display. "A" can be considered the value
		// and "B" as the format. This statement isn’t always true;
		// both registers are also used for calculating things before
		// it figures out what to display. This is why we see a
		// flickering display at times.
		//
		// On most calculators, what users know as the X, Y, Z and T
		// registers are the CPU C, D, E and F registers above. It
		// might be a little different for the HP-29C which needs
		// to retain information when the power is off.  
		//
		// P is a pointer. It goes from 0-13 and points into part of
		// a register i.e. at one of the 14 digits.
		//
		// S is a set of flags. These are used to remember conditions
		// like "the ‘f’ key was pressed" but some are also connected
		// to hardware; e.g. the W/PRGM-RUN switch or the low battery
		// sensor.
		//
		// These calculators have RAM chips for program and data
		// storage. There are 16 memories in a RAM chip and each
		// contains 14 BCD digits. Each digit is 4 bits so, in modern
		// terms, there are 112 bytes in a RAM chip.
		//
		// In the debugger of the javascript implementation, you can
		// use "dd" and a number to see what’s in the RAM chips.
		// 0 shows you registers 0-15, 1 16-31, and so on.
		//
		// There will always be a pattern that relates STO/RCL memory
		// numbers to data storage register numbers, and it is often
		// simple, but it was the designer’s choice of how the two
		// numbers relate. Don’t just assume (i) register 18 will be
		// in RAM chip 1 ie data register 18. It might be or might not.
		//
		// With the HP-67, the card reader circuit includes two data
		// registers used for transferring bits to a card and from a
		// card. These show up, in the HP-67, as if they were RAM
		// chip 9. You can view these during or after a card
		// read / write operation. You can also view them at other
		// times but they’re pretty boring – zeros – if they haven’t
		// been used.
		std::array<uint8_t, MAX_RAM_COLS> m_a;
		std::array<uint8_t, MAX_RAM_COLS> m_b;
		std::array<uint8_t, MAX_RAM_COLS> m_c;
		std::array<uint8_t, MAX_RAM_COLS> m_d;
		std::array<uint8_t, MAX_RAM_COLS> m_e;
		std::array<uint8_t, MAX_RAM_COLS> m_f;
		std::array<uint8_t, MAX_RAM_COLS> m_m1;
		std::array<uint8_t, MAX_RAM_COLS> m_m2;

		// s[3] & s[11] = PRGM memory status
		std::array<uint8_t, MEM_STATUS_SZ> m_s;

		int m_p; // Pointer into the nibbles of a register.
		int m_carry;
		int m_prevCarry;

		RAM m_ram;

		int m_ram_addr;

		bool m_display_14_digit;

		int m_pc;
		int m_bank;
		int m_dly_romchip_flag;
		int m_dly_romchip;
		int m_arithmetic_base;
		int m_p_change[P_CHANGE_SZ];
		int m_return_stack[RETURN_STACK_SZ]; // stack is 2 deep.
		int m_sp;

		int m_crc_a_key;
		int m_crc_pause;
		int m_crc_display_digits;
		int m_crc_card_inserted;
		int m_crc_merge;
		int m_crc_def_fn;

		// 1700 progress / state
		int m_crc_record;
		int m_crc_block;
		int m_crc_write_card;

		// 3 sync, 1 status, 16 regs of lhs+rhs, 2 chksum = 266 bytes
		// But actually, all the values are just (4-bit) nibbles.
		uint8_t m_crc_rawdata[CRC_RAWDATA_SZ];

		int m_busy; // delayed s[11] flag by 'busy' microinstructions
		int m_buffer;
		int m_offset;
};

#endif // _AAR_WOODSTOCK_H_

// HP67u Calculator Emulator - skin
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "skin.h"

#include "calcmain.h"
#include "filesys4.h"
#include "magcard.h"
#include "mainwindow.h"

#include "page_3.h"

// The original javascript code displays various pages.  Page 1 is the main
// calculator page, and the rest are essentially dialogs to do special
// functions.  Here is a list:
//
// Page 1 - main calculator page
// Page 2 - unused
// Page 3 - allows a memory card (disk file) to be selected.
// Page 4 - edit mag card text.
// Page 5 - entered from internals menu to look at the data memory.
// Page 6 - entered from internals menu to examine/modify the program memory.
// Page 7 - unused
// Page 8 - allows read/write access to internal data, accessed through page 9
//          (memory and program counter button)
// Page 9 - microcode debugger, allows single step, logging, etc.

skin::skin()
{
	//DEBUG_printf("skin constructed");
}

skin::~skin()
{
	//DEBUG_printf("skin destroyed");
}

void
skin::init(CLASSES *cl)
{
	m_cl = cl;

	m_skPower = true;
	m_skScale = 1;
	m_skWpgm = false;
	m_skCardName = "";
	m_skCardSide = 1;
	m_skLogMode = 0;
}

void
skin::showPage(QString id)
{
	//DEBUG_printf("id=%s", qPrintable(id));
}


void
skin::skSetScaleAndCenterDiv(QString id, int w, int h)
{
	//DEBUG_printf("id=%s w=%d h=%d", qPrintable(id), w, h);
}

int
skin::sz(int n)
{
	n *= m_skScale;
	return n;
}

// **** Page1 - Calculator

// Toggle the state of the power switch.
void
skin::OnPage1Power()
{
	m_cl->mag->setHelp("");
	updCardDisplay(false);	// card disappears at power off, stays gone at power on.

	m_skPower = !m_skPower;
	//DEBUG_printf("flip m_skPower to %d", m_skPower);
	if(m_skPower) {
		m_cl->mw->powerSwitch(SWITCH_RIGHT);
		m_cl->cm->doPowerOn();

		// comes up in run mode so ...
		if(m_skWpgm) {
			m_cl->cm->doWpgmOn();
		}
	} else {
		m_cl->mw->powerSwitch(SWITCH_LEFT);
		m_cl->cm->doPowerOff();
	}
}

void
skin::OnPage1Wpgm()
{
	// When changing modes, forget the open card, if any.
	if(m_cardFile.isOpen()) {
		m_cardFile.close();
	}
	m_skCardName = "";
	
	m_skWpgm = !m_skWpgm;
	//DEBUG_printf("flip m_skWpgm to %d", m_skWpgm);

	if(m_skWpgm) {
		m_cl->mw->runSwitch(SWITCH_LEFT);
		m_cl->cm->doWpgmOn();
	} else {
		m_cl->mw->runSwitch(SWITCH_RIGHT);
		m_cl->cm->doWpgmOff();
	}
}

void
skin::chooseCard()
{
	// Open a dialog to choose the side.
	Page_3 dialog;

	// Give page 3 access to the various classes.
	dialog.init(m_cl, m_skCardName);

	// Run the dialog.
	dialog.exec();
}

void
skin::setSide(int side, QString fn)
{
	//DEBUG_printf("Use side %d, fn %s", side, qPrintable(fn));

	// We need a file name.
	m_skCardName = fn;
	if(m_skCardName == "") {
		return;
	}

	// Set the side, and insert the card.
	m_skCardSide = side;
	m_cl->cm->doCardInsert();
}

// **** Startup

// Main entry point, called from html in the javascript version,
// and from main() in my version.
void
skin::startup()
{
	//DEBUG_printf("starting up");
	m_cl->cm->calcStartup();
	//DEBUG_printf("set m_skPower true");
	m_skPower = true;
	m_skWpgm = false;
	m_cl->cm->doPowerOn();
}

// **** Misc

// **** Updates from CalcMain

void
skin::updDisplay(QString s)
{
	emit sendDigit(s);
}

void
skin::updRegisters(std::array<QString, REG_LIST_SIZE> a)
{
	QString nl = "\n";

	QString s = "";
	s += "A  = " + a[ 1] + "    F  = " + a[10] + nl;
	s += "B  = " + a[ 2] + "    E  = " + a[ 9] + nl;
	s += "C  = " + a[ 3] + "    D  = " + a[ 8] + nl;
	s += "M1 = " + a[11] + "    M2 = " + a[12] + nl;
	s += "S  = " + a[ 4] + "  P  = " + a[ 0] + nl + nl;
	s += a[ 5] + " " + a[ 6] + nl;

	// If the uCode debugger is open, this will update the displayed
	// registers.
	emit sendRegisters(s);
}

void
skin::openLog()
{
	QFileDialog fileDialog;
	fileDialog.setFileMode(QFileDialog::AnyFile);

	QString strFile = fileDialog.getSaveFileName(NULL, "Create/open log file", QDir::homePath(), "");
	if(strFile.isEmpty()) {
		return;
	}

	m_logFile.setFileName(strFile);

	if(m_logFile.open(QIODevice::WriteOnly | QIODeviceBase::Append) == false) {
		//DEBUG_printf("cannot open %s", strFile.toLocal8Bit().data());
		return;
	}

	m_skLog.setDevice(&m_logFile);
	m_skLog << "HP67 Logging started\n\n";
}

void
skin::closeLog()
{
	m_logFile.close();
}

int
skin::getLogMode()
{
	return m_skLogMode;
}

void
skin::setLogMode(int i)
{
	switch(i) {
		case 0: // Log is off.
			m_skLogMode = i;
			closeLog();
			break;

		case 1: // Log is on.

			// Slow down so we don't overrun the log.
			if(m_cl->cm->isRunning()) {
				m_cl->cm->setSpeedSlower();
			}

			openLog();
			m_skLogMode = i;
			break;

		case 2: // log is detailed
			m_skLogMode = i;
			break;

		case 3: // log is smart
			m_skLogPrev = { // 13 registers to log, start off as 0.
				"0", "0", "0", "0",
				"0", "0", "0", "0",
				"0", "0", "0", "0",
				"0",
			};
			m_skLogMode = i;
			break;
	}

	emit refreshPage9();

}

void
skin::updLog(std::array<QString, REG_LIST_SIZE> a)
{
	int i, j;
	char buf[SMALL_BUFFER];

	if(!m_logFile.isOpen()) {
		m_skLogMode = 0;
		return;
	}

	if(m_skLogMode == 3) {
		QString s = "";
		int regs[] = { 1, 2, 3, 4, 0, 11, 12, 8, -1 }; // order for mode 3

		// PC and instruction
		if(m_skLogPrev[6] != "0") { // if just turned on SMART logging there isn't a prev instr
			s = m_skLogPrev[5] + " " + m_skLogPrev[6];
		}

		// changes
		bool first = true;
		for(i = 0; regs[i] != -1; i++) {
			int x = regs[i];
			QString names = "A= B= C= S= P= M1=M2=D= ";
			QString name = names.mid(3 * i, 3);
			if(a[x] != m_skLogPrev[x]) {
				if(first) {
					for(j = s.length(); j < 40; j++) {
						s += " ";
					}
					s += " ;";
					first = false;
				}
				s += " " + name + a[x];
			}
		}
		m_skLogPrev = a;

		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable(s));
		m_skLog << buf;
	}

	if(m_skLogMode == 2) {
		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable("A=" + a[ 1] + " F=" + a[10] + " M1=" + a[11]));
		m_skLog << buf;
		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable("B=" + a[ 2] + " E=" + a[ 9] + " M2=" + a[12]));
		m_skLog << buf;
		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable("C=" + a[ 3] + " D=" + a[ 8] + " S="  + a[ 4] + " P=" + a[0]));
		m_skLog << buf;
		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable(""   + a[ 5] + " "   + a[ 6])); // IP instr
		m_skLog << buf;
	}

	if(m_skLogMode == 1) {
		snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable(""   + a[ 5] + " "   + a[ 6])); // IP instr
		m_skLog << buf;
	}
}

void
skin::updLogText(QString msg)
{
	char buf[SMALL_BUFFER];

	if(!m_logFile.isOpen()) {
		m_skLogMode = 0;
		return;
	}

	snprintf(buf, SMALL_BUFFER, "%s\n", qPrintable("*** " + msg)); // IP instr
		m_skLog << buf;
}

void
skin::updCardDisplay(bool on)
{
	//DEBUG_printf("turn %s", on ? "on" : "off");
	if(!on) {
		m_cl->mag->clear();
	}

	emit cardImage(on);
	if(on) {
		emit cardTitle(m_cl->mag->getTitle());
		emit cardLabels(m_cl->mag->getLabels());
		emit forcedResize();
	}
}

// The fs->load() call will return something like:
//
// SIDE1
// 38
// 0000000
// 0000000
// 0000000
// 3100222
// 0e30fa1
// c3001fc
// 4737313
// ...
// SIDE2
// 0
// CARD
// 11
// Title: a2-1_polar_complex_four_functions
// A: X<>Y
// B: Z1+Z2
// C: 1/Z1
// D: Z1*Z2
// E: 
// a: 
// ...
// HELP
// 2
// This is some help.
// And more help.
// END
void
skin::updCardRead(uint8_t *a)
{
	bool rv;

	//DEBUG_printf("updCardRead");

	int i;
	SIDE_TYPE side;

	// Read SIDE1/SIDE2 text strings from a file.
	QString s = m_cl->fs->load(m_skCardName);
	if(s.isEmpty()) {
		s = "";
	}

	// Parse the string and call mag->loadSection with the parsed data.
	// This populates the magcard private data.
	rv = m_cl->mag->fmString(s);

	// The side chosen will have a length of 266, as will the "a" array.
	side = (m_skCardSide == 1) ? m_cl->mag->getSide(1) : m_cl->mag->getSide(2);

	for(i = 0; i < CRC_RAWDATA_SZ; i++) {
		// If the card is invalid, force all the bytes to zero.
		a[i] = rv ? side[i] : 0;
	}
}

void
skin::updCardWrite(uint8_t *a)
{
	//DEBUG_printf("write card");

	int i;

	SIDE_TYPE side;

	for(i = 0; i < CRC_RAWDATA_SZ; i++) {
		side[i] = a[i];
	}

	// Put data into a side, and mark it as populated.
	// This won't affect the other side.
	m_cl->mag->setSide(m_skCardSide, side);

	m_cl->fs->save(m_skCardName, m_cl->mag->crdToTxt());
}

// HP67u Calculator Emulator - AAR module
//
// Copyright (c) 2023 Steven A. Falco
//
// Copyright (c) 2015-2018 Greg Sydney-Smith
// 
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "aar.h"

#include "calcmain.h"
#include "controlAndTiming.h"

aar_woodstock::aar_woodstock()
{
	//DEBUG_printf("aar_woodstock constructed");
}

aar_woodstock::~aar_woodstock()
{
	//DEBUG_printf("aar_woodstock destroyed");
}

void
aar_woodstock::init(CLASSES *cl)
{
	m_cl = cl;

	// Clear internal registers.
	m_a = {};
	m_b = {};
	m_c = {};
	m_d = {};
	m_e = {};
	m_f = {};
	m_m1 = {};
	m_m2 = {};
	m_s = {}; // s[3] & s[11] = PRGM memory status

	m_p = 0;
	m_carry = 0;
	m_prevCarry = 0;

	// Clear ram (data registers & prog).
	memset(m_ram.ram, 0, sizeof(m_ram.ram));

	m_ram_addr = 0;
	m_display_14_digit = false;

	m_pc = 0;
	m_bank = 0;
	m_dly_romchip_flag = 0;
	m_dly_romchip = 0;
	m_arithmetic_base = DECIMAL_BASE;

	memset(m_p_change, 0, sizeof(m_p_change));
	memset(m_return_stack, 0, sizeof(m_return_stack));

	m_sp = 0;

	m_crc_a_key = false; // card reader circuit flag "a key was pressed"? 400 to set. 500 to check
	m_crc_pause = false; // ditto. start pause. check if pause complete.
	m_crc_display_digits = false;
	m_crc_card_inserted = false;
	m_crc_merge = false;
	m_crc_def_fn = false;	// default functions flag. ie no program loaded. A-E do 1/x etc.

	m_crc_record = 0; // 0=write sync nibbles, 1+ = write data from mem[0x99][7..13]
	m_crc_block = 0;
	m_crc_write_card = false; // crc mode: write data or prgm

	memset(m_crc_rawdata, 0, sizeof(m_crc_rawdata));

	m_busy = 0;
	m_buffer = 0;
	m_offset = 0;
}

int
aar_woodstock::getCarry()
{
	return m_carry;
}

void
aar_woodstock::setCarry(int n)
{
	m_carry = n;
}

int
aar_woodstock::getPrevCarry()
{
	return m_prevCarry;
}

void
aar_woodstock::setPrevCarry(int n)
{
	m_prevCarry = n;
}

int
aar_woodstock::getPchange(int n)
{
	return m_p_change[n];
}

void
aar_woodstock::setPchange(int n, int v)
{
	m_p_change[n] = v;
}

RAM *
aar_woodstock::getRAM()
{
	return &m_ram;
}

void
aar_woodstock::setRAM(RAM *r)
{
	m_ram = *r;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegA()
{
	return m_a;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegB()
{
	return m_b;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegC()
{
	return m_c;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegD()
{
	return m_d;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegE()
{
	return m_e;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegF()
{
	return m_f;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegM1()
{
	return m_m1;
}

std::array<uint8_t, MAX_RAM_COLS>
aar_woodstock::getRegM2()
{
	return m_m2;
}

int
aar_woodstock::getP()
{
	return m_p;
}

uint8_t
aar_woodstock::getRegStatus(int element)
{
	if(element < 0 || element >= MEM_STATUS_SZ) {
		return 0;
	}

	//DEBUG_printf("m_s[%d] is %d", element, m_s[element]);
	return m_s[element];
}

void
aar_woodstock::setRegStatus(int element, uint8_t value)
{
	if(element < 0 || element >= MEM_STATUS_SZ) {
		return;
	}

	m_s[element] = value;
	//DEBUG_printf("m_s[%d] <== %d (ext)", element, value);
}

// ROM routines, a/k/a helper functions

int
aar_woodstock::doAdd(int x, int y)
{
	int res = x + y + m_carry;

	m_carry = 0;
	if(res >= m_arithmetic_base) {
		res -= m_arithmetic_base;
		m_carry = 1;
	} 

	return res;
}

int
aar_woodstock::doSub(int x, int y)
{
	int res = (x - y) - m_carry;

	m_carry = 0;
	if(res < 0) {
		res += m_arithmetic_base;
		m_carry = 1;
	}

	return res;
}

int
aar_woodstock::field(int val)
{
	return (val == -1) ? m_p : val;
}

// any instructions taking arguments return a closure of no args

void
aar_woodstock::disptoggle()
{
	bool v = !m_cl->cat->getDisplay();

	// display toggle
	m_cl->cat->setDisplay(v);
	//DEBUG_printf("toggle %s", v ? "true" : "false");
}

void
aar_woodstock::dispoff()
{
	// display off
	m_cl->cat->setDisplay(false);
	//DEBUG_printf("off");
}

void
aar_woodstock::op_if_nc_goto(int addr)
{
	// woodstock if n/c go to addr
	if(m_prevCarry == 1) {
		// only does s/thing if n/c
		//DEBUG_printf("prev carry set");
		return;
	}

	m_pc = (m_pc & ~0377) | (addr & 0377);
	//DEBUG_printf("addr is %d new pc is %d", addr, m_pc);

	if(m_dly_romchip_flag) {
		m_pc = (m_dly_romchip << 8) + (m_pc & 0377);
		m_dly_romchip_flag = false;
	}
}

void
aar_woodstock::op_then_goto(int addr)
{
	// woodstock if ... then go to addr
	if(m_prevCarry == 1) {
		// only if n/c
		//DEBUG_printf("prev carry set");
		return;
	}

	m_pc = (m_pc & ~01777) | (addr & 01777);
	//DEBUG_printf("new address %d", m_pc);
}

void
aar_woodstock::op_jsb(int addr)
{
	// woodstock
	m_return_stack[m_sp] = m_pc;

	m_sp++;
	if(m_sp >= RETURN_STACK_SZ) {
		m_sp = 0;
	}

	m_pc = (m_pc & ~0377) | (addr & 0377);

	if(m_dly_romchip_flag) {
		m_pc = (m_dly_romchip << 8) + (m_pc & 0377);
		m_dly_romchip_flag = false;
	}
}

void
aar_woodstock::op_return()
{
	m_sp--;
	if(m_sp < 0) {
		m_sp = 1;	// STACK_SIZE - 1;
	}
	m_pc = m_return_stack[m_sp];
}

// nnnn 0011 00 (0014) 0 -> s N
// nnnn 0001 00 (0004) 1 -> s N
void
aar_woodstock::op_set_s(int num, int val)
{
	//DEBUG_printf("status before %s", qPrintable(statusToString()));
	if(num == 15 && val == 0 && m_cl->cm->getCalcKeyDown()) {
		//DEBUG_printf("cannot clear s15");
		return;	//2015-11-08. you can't clear s15 if you're holding a key down
	}
	m_s[num] = val;
	//DEBUG_printf("m_s[%d] <== %d (int)", num, val);
}

// nnnn 0111 00 (0034) if 0 = s N 
void
aar_woodstock::op_if_s_eq_0(int N)
{
	m_carry = (m_s[N] != 0) ? 1 : 0;
	//DEBUG_printf("test m_s[%d] eq 0, m_carry is now %d", N, m_carry);
}

// nnnn 0101 00 (0024) if 1 = s N 
void
aar_woodstock::op_if_s_eq_1(int N)
{
	m_carry = (m_s[N] == 0) ? 1 : 0;
	//DEBUG_printf("test m_s[%d] eq 1, m_carry is now %d", N, m_carry);
}

void
aar_woodstock::setp(int val)
{
	// # -> m_p
	m_p = val;
}

#if 0
// xxxx 1111 00 m_p <- N // x=f(N)
void
aar_woodstock::op_set_p(int N)
{
	m_p = N;
}

void
aar_woodstock::testp(int num)
{
	m_carry = (m_p == num) ? 1 : 0; // if m_p # 11
}
#endif

// xxxx 1011 00 (0054) if m_p # N // x=f(N)
void
aar_woodstock::op_if_p_ne(int N)
{
	// if m_p != N then nc (aka carry=0)
	if((N == 0) && (m_p_change[1] == 1) && (m_p_change[2] == 1)) {
		m_carry = ((m_p == 0) || (m_p == 1)) ? 1 : 0;
	} else if((N == 0) && (m_p_change[1] == -1) && (m_p_change[2] == -1)) {
		// debug("test m_p = 0 after double decrement");
		m_carry = ((m_p == 0) || (m_p == (MAX_RAM_COLS - 1))) ? 1 : 0;
	} else {
		m_carry = (m_p != N) ? 0 : 1;
	}
}

// xxxx 1001 00 (0044) if m_p = N // x=f(N)
void
aar_woodstock::op_if_p_eq(int N)
{
	if((N == 0) && (m_p_change[1] == 1) && (m_p_change[2] == 1)) {
		m_carry = ((m_p == 0) || (m_p == 1)) ? 0 : 1; // debug("match m_p=0 or m_p=1");
	} else if((N == 0) && (m_p_change[1] == -1) && (m_p_change[2] == -1)) {
		// debug("test m_p = 0 after double decrement");
		m_carry = ((m_p == 0) || (m_p == (MAX_RAM_COLS - 1))) ? 0 : 1;
	} else {
		m_carry = (m_p == N) ? 0 : 1;
	}
}

#if 0
void
aar_woodstock::incp()
{
	// m_p + 1 -> m_p
	m_p = (m_p + 1) & 0xf;
}
#endif

// 0111 0100 00 (0720) m_p + 1 -> m_p
void
aar_woodstock::op_inc_p()
{
	m_p_change[0] = 1;
	m_p++;
	if(m_p >= 14) {
		m_p=0;
	}
}

void
aar_woodstock::decp()
{
	// m_p - 1 -> m_p
	m_p = (m_p - 1) & 0xf;
}

// 0110 0100 00 (0620) m_p - 1 -> m_p
void
aar_woodstock::op_dec_p()
{
	m_p_change[0] = -1;
	if(m_p == 0) {
		m_p = 14;
	}
	m_p--;
}

// 00011 fff 10 a -> b[f] 
// 00101 fff 10 c -> a[f] 
// 00110 fff 10 b -> c[f] 
void
aar_woodstock::setreg(
		std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b,
		int first,
		int last
		)
{
	// b -> a[f] // ok
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	for(int i = f; i <= l; i++) {
		(*a)[i] = (*b)[i];
	}
}

#if 0
void
aar_woodstock::zeroreg(std::array<uint8_t, MAX_RAM_COLS> reg, int first, int last)
{
	// 0 -> r[f]
	setreg(reg, zeros, first, last);
}
#endif

// 01000 fff 10 (0402) 0 -> c[f]
// 00001 fff 10 (0042) 0 -> b[f]
// 00000 fff 10 (0002) 0 -> a[f]
void
aar_woodstock::op_set_reg_zero(
		std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	int f = field(first);
	int l = field(last);

	for(int i = f; i <= l; i++) {
		(*r)[i] = 0;
	}
	m_carry = 0;
}

// 0 - c -> c[f]
void
aar_woodstock::negc(int first, int last)
{
	negcdec(first, last, 0);
}

// 0 - c - 1 -> c[f]
void
aar_woodstock::negsubc(int first, int last)
{
	negcdec(first, last, 1);
}

void
aar_woodstock::nop()
{
}

#if 0
// 10100 fff 10 (1202) 0 - c -> c[f] // arith 0x14
void
aar_woodstock::op_negc(int first, int last)
{
	// NEW
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	int temp = 0;
	for(int i = f; i <= l; i++)
		c[i] = do_sub(temp, c[i]);
}
#endif

#if 0
// 10101 fff 10 (1242) 0 - c - 1 -> c[f] // arith 0x15
void
aar_woodstock::op_negsubc(int first, int last)
{
	// NEW
	int f = field(first);
	int l = field(last);

	m_carry = 1;
	int temp = 0;
	for(int i = f; i <= l; i++)
		c[i] = do_sub(temp, c[i]);
}
#endif

void
aar_woodstock::op_stack_to_a()
{
	// stack -> a	// ok
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_a[i] = m_d[i];
		m_d[i] = m_e[i];
		m_e[i] = m_f[i];
	}
}

// 01101 fff 10 a + 1 -> a[f] 
// 01111 fff 10 c + 1 -> c[f] 
void
aar_woodstock::increg(std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	int f = field(first);
	int l = field(last);

	m_carry = 1;
	for(int i = f; i <= l; i++) {
		(*r)[i] = doAdd((*r)[i], 0);
	}
}

// 10010 fff 10 a - 1 -> a[f] 
// 10011 fff 10 c - 1 -> c[f] 
void
aar_woodstock::decreg(std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	int f = field(first);
	int l = field(last);

	m_carry = 1;
	for(int i = f; i <= l; i++) {
		(*r)[i] = doSub((*r)[i], 0);
	}
}

// a exchange b[f] 
// a exchange c[f] 
// b exchange c[f] 
void
aar_woodstock::exchreg(std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b,
		int first,
		int last
		)
{
	// ok
	int f = field(first);
	int l = field(last);
	int t;

	m_carry = 0;
	for(int i = f; i <= l; i++) {
		t = (*a)[i];
		(*a)[i] = (*b)[i];
		(*b)[i] = t;
	}
}

void
aar_woodstock::negcdec(int first, int last, int dec)
{
	int f = field(first);
	int l = field(last);

	m_carry = dec;
	for(int i = f; i <= l; i++) {
		m_c[i] = doSub(0, m_c[i]);
	}
}

void
aar_woodstock::downrot()
{
	// down rotate	// ok
	int t;

	for(int i = 0; i < MAX_RAM_COLS; i++) {
		t = m_c[i];
		m_c[i] = m_d[i];
		m_d[i] = m_e[i];
		m_e[i] = m_f[i];
		m_f[i] = t;
	}
};

#if 0
void
aar_woodstock::clearregs()
{
	// not used
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		a[i] = b[i] = c[i] = d[i] = e[i] = f[i] = 0;
	}
}
#endif

#if 0
void
aar_woodstock::clears()
{
	for(int i = 0; i < MEM_PARTIAL_STATUS_SZ; i++) {
		s[i] = 0;
	}
}
#endif

void
aar_woodstock::op_clear_s()
{
	//DEBUG_printf("clear status");
	for(int i = 0; i < MEM_STATUS_SZ; i++) { // SSIZE; i++)
		if((i != 1) && (i != 2) && (i != 5) && (i != 15)) {
			m_s[i] = 0;
		}
	}
}

// 01110 fff 10 shift left a[f] 
void
aar_woodstock::shiftl(
		std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	// shift left r[f]	// ok
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	for(int i = l; i >= f; i--) {
		(*r)[i] = (i == f) ? 0 : (*r)[i - 1];
	}
}

void
aar_woodstock::shiftr(
		std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	// shift right r[f]
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	for(int i = f; i <= l; i++) {
		(*r)[i] = (i == l) ? 0 : (*r)[i + 1];
	}
}

void
aar_woodstock::op_c_to_stack()
{
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_f[i] = m_e[i];
		m_e[i] = m_d[i];
		m_d[i] = m_c[i];
	}
}

// 10110 fff 10 if b[f] = 0 
// 10111 fff 10 if c[f] = 0 
void
aar_woodstock::op_if_r_eq_0(
		std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	// carry is always 0 before any instruction
	// if r[f] = 0
	int f = field(first);
	int l = field(last);

	m_carry = 0;	// gss mod
	for(int i = f; i <= l; i++) {
		m_carry |= ((*r)[i] != 0) ? 1 : 0;
	}
}

// 10000 fff 10 a - b -> a[f] 
// 10001 fff 10 a - c -> c[f] 
// 11100 fff 10 a - c -> a[f] 
void
aar_woodstock::sub(
		std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b,
		std::array<uint8_t, MAX_RAM_COLS> *c,
		int first,
		int
		last)
{
	// b - c -> a[f]
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	for(int i = f; i <= l; i++) {
		(*a)[i] = doSub((*b)[i], (*c)[i]);
	}
}

// 01001 fff 10 a + b -> a[f] 
// 01010 fff 10 a + c -> a[f] 
// 01011 fff 10 c + c -> c[f] 
// 01100 fff 10 a + c -> c[f] 
void
aar_woodstock::add(
		std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b,
		std::array<uint8_t, MAX_RAM_COLS> *c,
		int first,
		int last
		)
{
	// ok
	int f = field(first);
       	int l = field(last);
	m_carry = 0;
	for(int i = f; i <= l; i++)
		(*a)[i] = doAdd((*b)[i], (*c)[i]);
}

// 11000 fff 10 if a >= c[f] 
// 11001 fff 10 if a >= b[f] 
void
aar_woodstock::op_if_r_ge_r(
		std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b,
		int first,
		int last
		)
{
	// ok
	int f = field(first);
	int l = field(last);

	m_carry = 0;
	for(int i = f; i <= l; i++) {
		doSub((*a)[i], (*b)[i]); // called for side effect of setting carry bit
	}
}

#if 0
void
aar_woodstock::regsgte1(std::array<uint8_t, MAX_RAM_COLS>reg, int first, int last)
{
	// if a[f] >= 1
	int f = field(first);
	int l = field(last);

	m_carry = 1;
	for(int i = f; i <= l; i++) {
		m_carry &= (reg[i] == 0) ? 1 : 0;
	}
}
#endif

// 11010 fff 10 if a[f] # 0
// 11011 fff 10 if c[f] # 0
void
aar_woodstock::op_if_r_ne_0(
		std::array<uint8_t, MAX_RAM_COLS> *r,
		int first,
		int last
		)
{
	int f = field(first);
	int l = field(last);

	m_carry = 1;
	for(int i = f; i <= l; i++) {
		m_carry &= ((*r)[i] == 0) ? 1 : 0;
	}
}

void
aar_woodstock::loadconst(int num)
{
	if(m_p < MAX_RAM_COLS) {
		// protect extra-digit range [0..13]
		m_c[m_p] = num;
	}

	if(--m_p < 0) {
		// it's a 0..13 counter;
		m_p = MAX_RAM_COLS - 1;
	}
}

#if 0
void
aar_woodstock::dlyselrom(int num)
{
	m_dly_romchip = num;
}
#endif

// nnnn 1101 00 (0064) delayed rom N
void
aar_woodstock::op_del_sel_rom(int N)
{
	m_dly_romchip = N;
	m_dly_romchip_flag = true;
}

// woodstock data registers (STO RCL memories, & prog)

void
aar_woodstock::op_c_to_addr()
{
	// different in woodstock. select ram bank eg 0(-15) 16(-31) 32(-47) 48(-63)
	m_ram_addr = (m_c[1] << 4) + m_c[0];
}

void
aar_woodstock::op_clear_data_regs()
{
	// 01260 clear data registers
	int base = m_ram_addr & ~0x0f;

	for(int i = base; i < base + 16; i++) {
		for(int j = 0; j < MAX_RAM_COLS; j++) {
			m_ram.ram[i][j] = 0;
		}
	}
}

char *
aar_woodstock::fmtRam(uint8_t *r, int n)
{
	static char lookup[] = "0123456789abcdef";
	static char p[MAX_RAM_COLS + 1];

	char *q = p;

	for(int j = 0; j < n; j++) {
		// r13, r12 .. r0
		*q++ = lookup[r[(MAX_RAM_COLS - 1) - j]];
	}

	*q = 0;

	return p;
}

// (1360) c -> data
void
aar_woodstock::op_c_to_data()
{ 
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_ram.ram[m_ram_addr][i] = m_c[i];
	}

	//DEBUG_printf("c -> ram[0x%x] (=%s)", m_ram_addr, fmtRam(m_ram.ram[m_ram_addr], MAX_RAM_COLS));
}

void
aar_woodstock::op_data_to_c()
{
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_c[i] = m_ram.ram[m_ram_addr][i];
	}

	//DEBUG_printf("ram[0x%x] -> c (=%s)", m_ram_addr, fmtRam(m_ram.ram[m_ram_addr], MAX_RAM_COLS));
}

void
aar_woodstock::op_c_to_data_register(int N)
{
	// 0050+(N<<6) c -> data register N. N=0..15
	m_ram_addr &= ~0x0f;
	m_ram_addr += N;

	if(m_ram_addr >= MAX_RAM_ROWS) {
		alert("c -> data register N. Out of range: 0x%x\n", m_ram_addr);
		m_ram_addr=0;
	}

	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_ram.ram[m_ram_addr][i] = m_c[i];
	}

	//DEBUG_printf("c -> ram[0x%x] (=%s)", m_ram_addr, fmtRam(m_ram.ram[m_ram_addr], MAX_RAM_COLS));
}

void
aar_woodstock::op_data_register_to_c(int n)
{
	// data register -> c NN (N=1..15)
	m_ram_addr &= ~0x0f;
	m_ram_addr += n;

	if(m_ram_addr >= MAX_RAM_ROWS) {
		alert("data register -> c N. Out of range: 0x%x\n", m_ram_addr);
		m_ram_addr = 0;
	}

	for(int i = 0; i < MAX_RAM_COLS; i++) {
		m_c[i] = m_ram.ram[m_ram_addr][i];
	}

	//DEBUG_printf("ram[0x%x] -> c (=%s)", m_ram_addr, fmtRam(m_ram.ram[m_ram_addr], MAX_RAM_COLS));
}

#if 0
void
aar_woodstock::op_f_to_a()
{
	a[0] = _f;
}

void
aar_woodstock::op_f_exch_a()
{
	int t;

	t = a[0];
	a[0] = _f;
	_f = t;
}

void
aar_woodstock::op_keys_to_rom_addr()
{
	m_pc = m_pc & 0x0f00;

	if(m_dly_romchip >= 0) {
		m_pc = (m_dly_romchip << 8) + (m_pc & 0377);
		m_dly_romchip= -1;
	}

	int v = m_cl->cat->kbdGet();
	m_pc += v;
}
#endif

void
aar_woodstock::op_circulate_a_left()
{
	// 0520 rotate a left
	int temp = m_a[MAX_RAM_COLS - 1];

	for(int i = (MAX_RAM_COLS - 1); i > 0; i--) {
		m_a[i] = m_a[i - 1];
	}

	m_a[0] = temp;
}

void
aar_woodstock::op_binary()
{
	// 01000100 00 binary
	m_arithmetic_base = BINARY_BASE;
}

// CRC has a CPU facing side, which appears as a set of opcodes or additional
// R flags that can be set or test/clear-ed; but it also has a hardware facing
// side which connects to switches like card_inserted, write_protect, at_head.
void
aar_woodstock::crc_start()
{
	m_crc_card_inserted = true;

	// Inserting a card clears the write buffer and starts us at record 0.
	for(int j = 0; j < MAX_RAM_COLS; j++) {
		m_ram.ram[0x99][j] = 0;
	}

	m_crc_record = 0;
}

void
aar_woodstock::op_crc(int N)
{
	// xxxxxx00 00 +n. CRC N. N= [001-017]{60|00}
	switch(N) {
		case  300:	// Test the RUN/PRGM switch
			m_s[3] = m_cl->cat->getProgram() ? 1 : 0;
			//DEBUG_printf("Test RUN/PRGM = %d", m_s[3]);
			break;

		case  400:	// Set the "a key was pressed" flag, probably.
			m_crc_a_key = true;
			//DEBUG_printf("Set key pressed flag");
			break;

		case  500:	// Check the "a key was pressed" flag, probably
			m_s[3] = m_crc_a_key ? 1 : 0;
			m_crc_a_key = false;
			//DEBUG_printf("Test key pressed flag = %d", m_s[3]);
			break;

		case 1400:	// Guess: set "waiting for side 2" flag
			m_crc_pause = true;
			//DEBUG_printf("Set waiting for side 2");
			break;

		case 1500:	// Guess: check "waiting for side 2" flag
			m_s[3] = m_crc_pause ? 1 : 0;
			m_crc_pause = false;
			//DEBUG_printf("Test waiting for side 2 = %d", m_s[3]);
			break;

		case   60:	// set ?
			m_crc_display_digits = true;
			//DEBUG_printf("Set display digits");
			break;

		case  160:	// test ?
			m_s[3] = m_crc_display_digits ? 1 : 0;
			m_crc_display_digits = false;
			//DEBUG_printf("Test display digits = %d", m_s[3]);
			break;

		case  560:	// card insert detect
			m_s[3] = m_crc_card_inserted ? 1 : 0;
			//DEBUG_printf("Test card inserted = %d", m_s[3]);
			break;

		case 1200:	// set merge flag
			m_crc_merge = true; 
			//DEBUG_printf("Set merge flag");
			break;

		case 1300:	// test merge flag
			m_s[3] = m_crc_merge ? 1 : 0;
			m_crc_merge = false;
			//DEBUG_printf("Test merge flag = %d", m_s[3]);
			break;

		case 1000:	// set default functions flag. ie no program loaded. A-E do 1/x etc.
			m_crc_def_fn = true;
			//DEBUG_printf("Set default functions flag");
			break;

		case 1100:	// test default functions flag.
			m_s[3] = m_crc_def_fn ? 1 : 0;
			m_crc_def_fn = false;
			//DEBUG_printf("Test default functions flag = %d", m_s[3]);
			break;

		case  100:	// Check ready
			m_s[3] = 1;		// Device always ready
			m_crc_card_inserted = 0;	// side-effect clear card inserted flag
			//DEBUG_printf("Test ready flag = %d", m_s[3]);
			break;

		case  360:	// Motor off
			m_crc_record = 0; // motor off resets record pointer
			m_crc_block = 0;
			//DEBUG_printf("Motor off");
			break;

		case  260:	// Motor on
			//DEBUG_printf("Motor on");
			break;

		case  660:	// Set Write Mode
			m_crc_write_card = true;		// write mode (data or prgm)
			//DEBUG_printf("Set write card mode");
			break;

		case  760:	// Set Read Mode (probably really CRC.Init())
			m_crc_write_card = false;
			for(int j = 0; j < MAX_RAM_COLS; j++) {
				m_ram.ram[0x99][j] = 0; // 2015-11-11 and zero the write data register
			}
			//DEBUG_printf("Set read card mode - zeros ram at 0x99");
			break;

		case 1700:	// read/write card
			op_crc_1700();
			//DEBUG_printf("Read/write card");
			break;

		default:
			//DEBUG_printf("unknown code = %d", N);
			break;
	}
}

// 00010100 00. keys to a
void
aar_woodstock::op_keys_to_a()
{
	int key = m_cl->cat->kbdGet();

	m_a[2] = (key >> 4);
	m_a[1] = (key & 0x0f);
}

// 0010010 00. a -> rom address
void
aar_woodstock::op_a_to_rom_addr()
{
	// debug("Pc : " + Integer.toOctalString( (m_pc - 1 ) ));
	m_pc = m_pc & ~0xff;

	if(m_dly_romchip_flag) {
		m_pc = (m_dly_romchip << 8) + (m_pc & 0xff);
		m_dly_romchip_flag = false;
	}

	m_pc += ((m_a[2] << 4) + m_a[1]);
}

// 0011 0100 00 (0320) display reset twf
// this is only called once in the HP67 uCode.
// it seems to be to switch display modes on hp 21,25,29 etc.
// always on for hp67 as hp67 has full 15 digit display.
void
aar_woodstock::op_display_reset_twf()
{
	m_display_14_digit = true;
}

bool
aar_woodstock::getDisplay14Digit()
{
	return m_display_14_digit;
}

// 00410 m1 exch c
// 00610 m2 exch c
void
aar_woodstock::op_m_exch_c(std::array<uint8_t, MAX_RAM_COLS> *r)
{
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		int temp = m_c[i];
		m_c[i] = (*r)[i];
		(*r)[i] = temp;
	}
}

// 01010010 00  (0510) m1 -> c
// 01110010 00  (0710) m2 -> c
// 10100010 00 (01210) y -> a
void
aar_woodstock::op_r_to_r(
		std::array<uint8_t, MAX_RAM_COLS> *a,
		std::array<uint8_t, MAX_RAM_COLS> *b
		)
{
	for(int i = 0; i < MAX_RAM_COLS; i++) {
		(*b)[i] = (*a)[i];
	}
}

// 11000010 00 (01410) decimal
void
aar_woodstock::op_decimal()
{
	m_arithmetic_base = DECIMAL_BASE;
}

// 1000 1100 00 (01060) bank switch
void
aar_woodstock::op_bank_switch()
{
	m_bank ^= 1;
}

void
aar_woodstock::CardWrite()
{
	int j;

	// it always writes 7 nibbles from the left, ram[0x99] m13 m12 .. m7
	for(j = 0; j < NIBBLES; j++) {
		m_crc_rawdata[(m_crc_record * NIBBLES) + j] = m_ram.ram[0x99][(MAX_RAM_COLS - 1) - j];
	}

	//DEBUG_printf("CardWrite %d: %s", m_crc_record, fmtRam(m_ram.ram[0x99], NIBBLES));

	if(++m_crc_record == 38) {
		m_cl->cm->cardWrite(m_crc_rawdata);
	}
}

void
aar_woodstock::CardRead()
{
	int j;

	if(m_crc_record == 0) {
		m_cl->cm->cardRead(m_crc_rawdata);
		m_crc_record = 1; // first row (sync) is handled by hardware
	}

	for(j = 0; j < NIBBLES; j++) {
		m_ram.ram[0x9b][ 6 - j] = m_crc_rawdata[(m_crc_record * NIBBLES) + j];
		m_ram.ram[0x9b][13 - j] = m_crc_rawdata[(m_crc_record * NIBBLES) + j];
	}

	//DEBUG_printf("CardRead %d: %s", m_crc_record, fmtRam(m_ram.ram[0x9b], NIBBLES));

	m_crc_record++;
}

// Read/Write Card
void
aar_woodstock::op_crc_1700()
{
	bool progSw = m_cl->cat->getProgram();

	if(!progSw && m_crc_write_card) {
		// WRITE IN RUN MODE // "f W/DATA"
		CardWrite();
		m_s[3] = 0;
		//DEBUG_printf("clear m_s[3] - writing data");
	}

	if(!progSw && !m_crc_write_card) {
		// READ IN RUN MODE DEFAULT
		CardRead();
		m_s[3] = 0;
		m_crc_def_fn = false;
		//DEBUG_printf("clear m_s[3] - reading program/data");
	}

	if(progSw) {
		// PRGM mode : Write PRGM
		CardWrite();
		m_s[3] = 0;
		//DEBUG_printf("clear m_s[3] - writing program");
	}

	if(++m_crc_block == 38) {
		// # of calls 1700 per card
		m_crc_block = 0;
	}
}

int
aar_woodstock::getBank()
{
	return m_bank;
}

void
aar_woodstock::setBank(int n)
{
	m_bank = n;
}

int
aar_woodstock::getPC()
{
	return m_pc;
}

void
aar_woodstock::setPC(int n)
{
	m_pc = n;
}

int
aar_woodstock::getBusy()
{
	return m_busy;
}

void
aar_woodstock::setBusy(int n)
{
	m_busy = n;
}

int
aar_woodstock::getCRCdefFn()
{
	return m_crc_def_fn;
}

QString
aar_woodstock::regToString(std::array<uint8_t, MAX_RAM_COLS> r)
{
	int i;
	QString v;

	v = "";
	for(i = 0; i < MAX_RAM_COLS; i++) {
		if(i != 0) {
			v += ",";
		}
		v += std::to_string(r[i]);
	}

	return v;
}

QString
aar_woodstock::statusToString()
{
	int i;
	QString v;

	v = "";
	for(i = 0; i < MEM_STATUS_SZ; i++) {
		if(i != 0) {
			v += ",";
		}
		v += std::to_string(m_s[i]);
	}

	return v;
}


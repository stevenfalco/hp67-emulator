// HP67u Calculator Emulator
//
// File->Magcard->Card Access
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef PAGE_3_H
#define PAGE_3_H

#include <QDialog>

#include "global.h"

namespace Ui {
	class Page_3;
}

class Page_3 : public QDialog
{
	Q_OBJECT

	public:
		explicit Page_3(QWidget *parent = nullptr);
		~Page_3();

		void init(CLASSES *cl, QString fn);

	signals:
		void setSide(int side, QString fn);

	private:
		Ui::Page_3 *ui;

		CLASSES *m_cl;

		QString m_fileName;

		void displayFileName();

	private slots:
		void closer();
		void side();
		void chooseFile();
};

#endif // PAGE_3_H

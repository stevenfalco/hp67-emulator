// HP67u Calculator Emulator
//
// Internals->Banks
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_7.h"
#include "ui_page_7.h"

#include "calcmain.h"

Page_7::Page_7(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Page_7)
{
    ui->setupUi(this);

    QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
}

Page_7::~Page_7()
{
    delete ui;
}

void 
Page_7::init(CLASSES *cl)
{
	std::array<QString, MAX_RAM_ROWS> data;

	// "Internal" ram banks
	int banks = 4;
	int bankSize = 16;

	int bs = 128;
	char buf[bs];

	m_cl = cl;
		
	data = m_cl->cm->getAllData();

	QString s = "";

	s += " 0-15 RAM Bank 0 (STO/RCL 0-9,A-E,I)\n";
	s += "16-31 RAM Bank 1 (prog steps 224-113)\n";
	s += "32-47 RAM Bank 2 (prog steps 112-001)\n";
	s += "48-63 RAM Bank 3 (Secondary Regs 0-9 + 6 internal)\n\n";
	for(int i = 0; i < bankSize; i++) {
		snprintf(buf, bs, "%3d: %s | %3d: %s | %3d: %s | %3d: %s\n",
				i + (0 * bankSize), qPrintable(data[i + (0 * bankSize)]),
				i + (1 * bankSize), qPrintable(data[i + (1 * bankSize)]),
				i + (2 * bankSize), qPrintable(data[i + (2 * bankSize)]),
				i + (3 * bankSize), qPrintable(data[i + (3 * bankSize)]));
		s += buf;
	}

	s += "\nUnknown - possibly unused\n\n";
	for(int i = 0; i < bankSize; i++) {
		snprintf(buf, bs, "%3d: %s | %3d: %s | %3d: %s | %3d: %s\n",
				i + (4 * bankSize), qPrintable(data[i + (4 * bankSize)]),
				i + (5 * bankSize), qPrintable(data[i + (5 * bankSize)]),
				i + (6 * bankSize), qPrintable(data[i + (6 * bankSize)]),
				i + (7 * bankSize), qPrintable(data[i + (7 * bankSize)]));
		s += buf;
	}

	s += "\nCard Reader registers\n";
	s += "153 - Write Data\n";
	s += "155 - Read Data\n";
	s += "Others - unknown\n\n";
	for(int i = 0; i < bankSize; i++) {
		snprintf(buf, bs, "%3d: %s | %3d: %s\n",
				i + (8 * bankSize), qPrintable(data[i + (8 * bankSize)]),
				i + (9 * bankSize), qPrintable(data[i + (9 * bankSize)]));
		s += buf;
	}

	ui->text->setPlainText(s);
}

void
Page_7::closer()
{
	close();
}

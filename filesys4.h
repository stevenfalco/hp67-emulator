// HP67u Calculator Emulator - filesys4
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#ifndef _FILESYS4_H_
#define _FILESYS4_H_

#include "global.h"

// Not sure how large this should be.
#define BUF_SIZE 16384

class filesys4 : public QThread
{
	Q_OBJECT

	public:
		filesys4();
		~filesys4();

		QString	load(QString name);
		void		save(QString name, QString s);

	private:
};

#endif // _FILESYS4_H_

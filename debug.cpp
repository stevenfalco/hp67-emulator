// HP67u Calculator Emulator
//
// Copyright (c) 2023 Steven A. Falco
// 
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include <stdarg.h>
#include <stdio.h>

#include <syslog.h>
#include <time.h>

#include "global.h"
#include "debug.h"

// Write to stderr - this uses a format and stdarg so % will be honored.
void
_DEBUG_printf(const char *fn, int ln, const char *format, ...)
{
	va_list			ap;
	char			*part1;
	char			*part2;
	struct timespec		now;
	struct tm		*tm;

	if((part1 = (char *)malloc(HUGE_BUFFER)) == NULL) {
		return;
	}

	if((part2 = (char *)malloc(HUGE_BUFFER)) == NULL) {
		free(part1);
		return;
	}

	// Get the current time as a tm struct.
	clock_gettime(CLOCK_REALTIME, &now);
	if(now.tv_nsec >= 500000000) {
		now.tv_sec++;
	}
	tm = localtime(&now.tv_sec);

	// Load the time into the part1 buffer.
	snprintf(part1, HUGE_BUFFER, "%4d-%02d-%02d %02d:%02d:%02d %s:%d",
				tm->tm_year + 1900,
				tm->tm_mon + 1,
				tm->tm_mday,
				tm->tm_hour,
				tm->tm_min,
				tm->tm_sec,
				fn,
				ln);

	// Format the user message into the part2 buffer.
	va_start(ap, format);
	vsnprintf(part2, HUGE_BUFFER, format, ap);
	va_end(ap);

	// Send the combination to stderr.
	fprintf(stderr, "%s %s\n", part1, part2);
	fflush(stderr);

	free(part1);
	free(part2);
}

void alert(const char *format, ...)
{
	va_list			ap;
	char buf[SMALL_BUFFER];

	va_start(ap, format);
	vsnprintf(buf, SMALL_BUFFER, format, ap);
	va_end(ap);

	QMessageBox m;
	m.setIcon(QMessageBox::Warning);
	m.setText(buf);
	m.exec();
}

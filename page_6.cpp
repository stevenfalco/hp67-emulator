// HP67u Calculator Emulator
//
// Internals->Program
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_6.h"
#include "ui_page_6.h"

#include "calcmain.h"
#include "magcard.h"
#include "mainwindow.h"

Page_6::Page_6(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Page_6)
{
	ui->setupUi(this);

	QObject::connect(ui->Clear, SIGNAL(clicked()), this, SLOT(clear()));
	QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
}

Page_6::~Page_6()
{
	delete ui;
}

void 
Page_6::init(CLASSES *cl)
{
	m_cl = cl;

	m_cl->mag->setContent(m_cl->cm->getProg());
	
	m_text = m_cl->mag->toString();
	ui->text->setPlainText(m_text);

	QObject::connect(this, &Page_6::cardImage, m_cl->mw, &HP67::cardImage);
	QObject::connect(this, &Page_6::cardTitle, m_cl->mw, &HP67::cardTitle);
	QObject::connect(this, &Page_6::cardLabels, m_cl->mw, &HP67::cardLabels);
	QObject::connect(this, &Page_6::forcedResize, m_cl->mw, &HP67::forcedResize);
}

void
Page_6::clear()
{
	ui->text->clear();
}

void
Page_6::closer()
{
	QString text = ui->text->toPlainText();

#if 0
	for(int i = 0; i < text.length(); i++) {
		if(i % 16 == 0) {
			fprintf(stderr, "%3d:", i);
		}
		fprintf(stderr, " %02x", text[i]);
		if(i % 16 == 15) {
			fprintf(stderr, "\n");
		}
	}
	fprintf(stderr, "\n");
#endif

	if(text != "") {
		if(text != m_text) {
			if(!m_cl->mag->fmString(text)) {
				alert("Invalid card");
				close();
			}
			m_cl->cm->setProg(m_cl->mag->getContent());
			m_cl->cm->setData(m_cl->mag->getData());
			m_cl->cm->setSettings(m_cl->mag->getSettings());

			emit cardImage(true);
			emit cardTitle(m_cl->mag->getTitle());
			emit cardLabels(m_cl->mag->getLabels());
			emit forcedResize();
		}
	}

	close();
}

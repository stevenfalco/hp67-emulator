// HP67u Calculator Emulator
//
// File->Magcard->Edit Card Text
//
// Copyright (c) 2023 Steven A. Falco
// 
// Copyright (c) 2015-2018 Greg Sydney-Smith
//
// This builds on earlier work by Greg Sydney-Smith, Jacques Laporte,
// Eric Smith, David G. Hicks, and Ashley Feniello.
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.  This program is distributed in the hope that
// it will be useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU General Public License for more details.  You should have
// received a copy of the GNU General Public License along with this
// program. If not, see <https://www.gnu.org/licenses/>.

#include "page_4.h"
#include "ui_page_4.h"

#include "magcard.h"
#include "mainwindow.h"

Page_4::Page_4(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Page_4)
{
	ui->setupUi(this);

	QObject::connect(ui->Back, SIGNAL(clicked()), this, SLOT(closer()));
}

Page_4::~Page_4()
{
	delete ui;
}

void 
Page_4::init(CLASSES *cl)
{
	m_cl = cl;

	m_title = m_cl->mag->getTitle();
	m_labels = m_cl->mag->getLabels();
	m_help = m_cl->mag->getHelp();

	ui->lineEditTitle->setText(m_title);

	ui->lineEdit_A->setText(m_labels[0]);
	ui->lineEdit_B->setText(m_labels[1]);
	ui->lineEdit_C->setText(m_labels[2]);
	ui->lineEdit_D->setText(m_labels[3]);
	ui->lineEdit_E->setText(m_labels[4]);
	ui->lineEdit_a->setText(m_labels[5]);
	ui->lineEdit_b->setText(m_labels[6]);
	ui->lineEdit_c->setText(m_labels[7]);
	ui->lineEdit_d->setText(m_labels[8]);
	ui->lineEdit_e->setText(m_labels[9]);

	ui->plainTextEditHelp->setPlainText(m_help);

	QObject::connect(this, &Page_4::cardTitle, m_cl->mw, &HP67::cardTitle);
	QObject::connect(this, &Page_4::cardLabels, m_cl->mw, &HP67::cardLabels);
	QObject::connect(this, &Page_4::forcedResize, m_cl->mw, &HP67::forcedResize);
}

void
Page_4::closer()
{
	int i;
	bool update = false;

	QString title = ui->lineEditTitle->text();
	std::array<QString, MAX_LABELS> labels;
	QString help = ui->plainTextEditHelp->toPlainText();

	labels[0] = ui->lineEdit_A->text();
	labels[1] = ui->lineEdit_B->text();
	labels[2] = ui->lineEdit_C->text();
	labels[3] = ui->lineEdit_D->text();
	labels[4] = ui->lineEdit_E->text();
	labels[5] = ui->lineEdit_a->text();
	labels[6] = ui->lineEdit_b->text();
	labels[7] = ui->lineEdit_c->text();
	labels[8] = ui->lineEdit_d->text();
	labels[9] = ui->lineEdit_e->text();

	// We can only update the entire card so we just need one flag.
	if(m_title != title) {
		update = true;
	}

	for(i = 0; i < MAX_LABELS; i++) {
		if(labels[i] != m_labels[i]) {
			update = true;
			break;
		}
	}

	if(update) {
		// Update the title.
		m_cl->mag->setTitle(title);
		emit cardTitle(title);

		// Update the labels.
		m_cl->mag->setLabels(labels);
		emit cardLabels(labels);
		emit forcedResize();
	}

	if(help != m_help) {
		// The last character of the help must be a newline,
		// so add one if needed.
		if(help.length() > 0) {
			// There is something here.  Does it end with a newline?
			if(help[help.length() - 1] != '\n') {
				help += "\n";
			}
		}

		m_cl->mag->setHelp(help);
	}

	close();
}
